build:
	docker-compose build

.PHONY: build

test:
	cd src;\
	go test ./... -v -race

.PHONY: test

lint:
	cd src;\
	golangci-lint -v -c ../.golangci.yml run

.PHONY: lint

docker_deploy:
	docker-compose up --build

.PHONY: docker_deploy

docker_clean:
	docker-compose down
	docker volume rm nft-marketplace_postgres

.PHONY: docker_clean

docker_redeploy: docker_clean docker_deploy

.PHONY: docker_redeploy

go-list:
	cd src;\
	go list -json -deps ./... > ../go-list.json

.PHONY: go-list