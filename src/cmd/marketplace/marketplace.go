package main

import (
	"crypto/rand"
	"os"

	"gitlab.com/romko11l/nft-marketplace/src/internal/api"
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/hash"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/recovery_keys"
)

type sessionKeyGeneratorStub struct{}

func (s sessionKeyGeneratorStub) NewSessionKey() string {
	return ""
}

func main() {
	logger := log.NewLogger()

	connString := os.Getenv("POSTGRES_CONN_STRING")

	base, err := database.NewDataProvider(connString, hash.NewHasher(), recovery_keys.NewDefaultKeyGenerator())
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "couldn't connect to db"))
	}

	const sessionAuthKeyLen = 64
	sessionAuthKey := make([]byte, sessionAuthKeyLen)
	_, err = rand.Read(sessionAuthKey)
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "couldn't generate random session auth key"))
	}

	const sessionEncryptionKeyLen = 32
	sessionEncryptionKey := make([]byte, sessionEncryptionKeyLen)
	_, err = rand.Read(sessionEncryptionKey)
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "couldn't generate random session encryption key"))
	}

	port := os.Getenv("PORT")

	httpServantConfig := http.ServantConfig{
		Server: http.ServerConfig{
			Addr: ":" + port,
		},
		SessionStorage: http.SessionStorageConfig{
			AuthenticationKey: sessionAuthKey,
			EncryptionKey:     sessionEncryptionKey,
		},
	}

	servant, err := http.NewServant(
		httpServantConfig,
		base,
		logger,
		map[string]http.Handler{
			"/register":   http.NewSimpleHTMLWrapper(api.NewRegistrationHandler(base, hash.NewHasher(), sessionKeyGeneratorStub{})),
			"/login":      http.NewSimpleHTMLWrapper(api.NewAuthenticationHandler(base, hash.NewHasher())),
			"/recover":    http.NewSimpleHTMLWrapper(api.NewRecoveryHandler(base, hash.NewHasher())),
			"/home":       http.NewSimpleHTMLWrapper(api.NewHomePageHandler(base)),
			"/user":       http.NewSimpleHTMLWrapper(api.NewUserPageHandler(base)),
			"/users_info": http.NewSimpleHTMLWrapper(api.NewUsersInfoHandler(base)),
			"/feed":       http.NewSimpleHTMLWrapper(api.NewImagesFeedHandler(base)),
			"/":           http.NewSimpleHTMLWrapper(api.NewIndexHandler()),
			"/upload": http.NewSimpleHTMLWrapper(
				api.NewUploadHandler(
					hash.NewImageHasher(),
					base,
				),
			),
			"/image":             http.NewSimpleHTMLWrapper(api.NewImageHandler(base)),
			"/like_image":        api.NewlikeImageHandler(base),
			"/likes_amount":      api.NewLikesAmountHandler(base),
			"/change_privacy":    http.NewSimpleHTMLWrapper(api.NewChangePrivacyHandler(base)),
			"/my_images":         http.NewSimpleHTMLWrapper(api.NewMyImagesHandler(base)),
			"/set_avatar":        http.NewSimpleHTMLWrapper(api.NewSetAvatarHandler(base)),
			"/recovery_approval": http.NewSimpleHTMLWrapper(api.NewRecoveryApprovalHandler(base, hash.NewHasher())),
			"/logout":            http.NewSimpleHTMLWrapper(api.NewLogoutHandler()),
			"/transfer":          http.NewSimpleHTMLWrapper(api.NewTransferHandler(base)),
			"/transfer_approval": http.NewSimpleHTMLWrapper(api.NewTransferApprovalHandler(base)),
			"/upload_from_url": http.NewSimpleHTMLWrapper(api.NewUploadFromURLHandler(
				hash.NewImageHasher(),
				base,
			)),
			"/send_comment":      http.NewSimpleHTMLWrapper(api.NewCommentsHandler(base)),
			"/change_pub_rights": api.NewPubRightsPageHandler(base),
			"/complain_comment":           http.NewSimpleHTMLWrapper(api.NewCommentComplaintHandler(base)),
			"/comment_complaint_approval": http.NewSimpleHTMLWrapper(api.NewCommentComplaintApprovalHandler(base)),
			"/complain_image":    http.NewSimpleHTMLWrapper(api.NewComplainImageHandler(base)),
			"/images_complaints": http.NewSimpleHTMLWrapper(api.NewComplainImageApprovalHandler(base)),
		},
	)
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "couldn't create servant"))
	}

	logger.Info("Service started on port: " + port)

	err = servant.Serve()
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "servant error"))
	}
}
