package api

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"html/template"
	"strconv"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

type transferApprovalDatabase interface {
	GetImageTransferRequests(receiverUsername string) ([]dto.ImageTransferRequest, error)
	ApproveImageTransferRequest(senderUsername, receiverUsername string, imageID int) (bool, string, error)
	DenyImageTransferRequest(senderUsername, receiverUsername string, imageID int) error
	GetImageInfo(imageID int) (dto.Image, error)
}

func NewTransferApprovalHandler(
	database transferApprovalDatabase,
) *transferApprovalHandler {
	return &transferApprovalHandler{
		database: database,
	}
}

type transferApprovalHandler struct {
	database transferApprovalDatabase
}

func (h *transferApprovalHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *transferApprovalHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}
	user, ok := ctx.GetUser()
	if !ok {
		return nil, errors.Wrap(err, "no user")
	}

	form := request.PostForm
	sender := form.Get("sender")
	imageIDString := form.Get("imageID")
	imageID, err := strconv.Atoi(imageIDString)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse imageID")
	}

	action := form.Get("action")
	ret := ""
	switch action {
	case "approve":
		ok, newSecretKey, err := h.database.ApproveImageTransferRequest(sender, user.Username, imageID)
		if err != nil {
			return nil, errors.Wrap(err, "request approval failed")
		}
		if ok {
			ret = fmt.Sprintf("Approved successfully, image recovery key: %s", newSecretKey)
		} else {
			ret = "Approval failed (likely due to image ownership change since request creation)"
		}
	case "deny":
		err := h.database.DenyImageTransferRequest(sender, user.Username, imageID)
		if err != nil {
			return nil, errors.Wrap(err, "request deny failed")
		}
		ret = "Transfer request denied successfully"

	}
	return http.NewResponse([]byte(
		ret,
	), http.StatusOK), nil
}

type imageTransferRequestDisplay struct {
	dto.ImageTransferRequest
	ImageData string
}

func (h *transferApprovalHandler) serveHTTPGet(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return nil, errors.Error("no user")
	}
	template, err := template.ParseFiles("static/transfer_approval.html")
	if err != nil {
		return nil, err
	}

	requests, err := h.database.GetImageTransferRequests(user.Username)
	if err != nil {
		return nil, err
	}

	requestsDisplay := make([]imageTransferRequestDisplay, 0)
	for _, request := range requests {
		image, err1 := h.database.GetImageInfo(request.ImageID)
		if err1 != nil {
			return nil, err1
		}

		requestsDisplay = append(requestsDisplay, imageTransferRequestDisplay{
			ImageTransferRequest: request,
			ImageData:            base64.StdEncoding.EncodeToString(image.Body),
		})
	}

	body := new(bytes.Buffer)
	err = template.Execute(body, map[string]interface{}{
		"requests":       requestsDisplay,
		csrf.TemplateTag: csrf.TemplateField(request),
	})
	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}
