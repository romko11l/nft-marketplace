package api

import (
	"encoding/json"
	"strconv"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewlikeImageHandler(dataProvider *database.DataProvider) likeImageHandler {
	return likeImageHandler{dataProvider}
}

type likeImageHandler struct {
	dataProvider *database.DataProvider
}

type ImageID struct {
	ImageID string `json:"imageId"`
}

func (h likeImageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	if request.Method == http.MethodPost {
		var i ImageID
		err := json.NewDecoder(request.Body).Decode(&i)
		if err != nil {
			return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
		}

		imageID, err := strconv.Atoi(i.ImageID)
		if err != nil {
			return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
		}

		ok, err := h.dataProvider.LikeImage(user, imageID)
		if err != nil {
			return http.NewResponse([]byte("Bad request"), http.StatusBadRequest), nil
		}

		if !ok {
			return http.NewResponse([]byte("Forbidden"), http.StatusForbidden), nil
		}

		return http.NewResponse([]byte("OK"), http.StatusOK), nil
	} else {
		return http.NewResponse([]byte("Method not allowed"), http.StatusMethodNotAllowed), nil
	}
}

func NewLikesAmountHandler(dataProvider *database.DataProvider) likesAmountHandler {
	return likesAmountHandler{dataProvider}
}

type likesAmountHandler struct {
	dataProvider *database.DataProvider
}

func (h likesAmountHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	if request.Method == http.MethodGet {
		id := request.URL.Query().Get("image_id")
		imageID, err := strconv.Atoi(id)
		if err != nil {
			return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), nil
		}

		imageInfo, err := h.dataProvider.GetImageInfo(imageID)
		if err != nil {
			return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), nil
		}

		if imageInfo.IsPrivate {
			return http.NewResponse([]byte("Forbidden"), http.StatusForbidden), nil
		}

		return http.NewResponse([]byte(strconv.Itoa(len(imageInfo.Likes))), http.StatusOK), nil
	} else {
		return http.NewResponse([]byte("Method not allowed"), http.StatusMethodNotAllowed), nil
	}
}
