package api

import (
	"strconv"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewSetAvatarHandler(dataProvider *database.DataProvider) setAvatarHandler {
	return setAvatarHandler{dataProvider}
}

type setAvatarHandler struct {
	dataProvider *database.DataProvider
}

func (h setAvatarHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	imageIDstr := request.FormValue("image_id")
	if imageIDstr == "" {
		return http.NewResponse([]byte("404, Not Found"), http.StatusNotFound), nil
	}

	imageID, err := strconv.Atoi(imageIDstr)
	if err != nil {
		return nil, err
	}

	err = h.dataProvider.SetUserAvatar(user, imageID)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	return http.NewResponse([]byte("Avatar set successfully"), http.StatusOK), nil
}
