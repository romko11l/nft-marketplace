package api

import (
	"fmt"
	"strconv"

	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

const (
	transferImageIDField  = "image_id"
	transferReceiverField = "receiver"
	transferCaptchaField  = "captcha"
)

type transferDatabase interface {
	IsUserOwnerOfImage(username string, imageID int) (bool, error)
	DoesUserExist(username string) (bool, error)
	RequestImageTransfer(senderUsername, receiverUsername string, imageID int) (bool, error)
}

func NewTransferHandler(
	database transferDatabase,
) *transferHandler {
	return &transferHandler{
		database: database,
	}
}

type transferHandler struct {
	database transferDatabase
}

func (h *transferHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("authorization required"), http.StatusUnauthorized), nil
	}
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *transferHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm

	captcha := form.Get(transferCaptchaField)
	ok, err := http.ValidateCaptcha(ctx, captcha)

	if !ok {
		return nil, errors.Error("couldn't validate captcha")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	imageIDString := form.Get(transferImageIDField)
	imageID, err := strconv.Atoi(imageIDString)
	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "couldn't parse image id").Error()), http.StatusUnprocessableEntity), nil
	}

	receiver := form.Get(transferReceiverField)

	user, _ := ctx.GetUser()

	ok, err = h.database.IsUserOwnerOfImage(user.Username, imageID)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check if user is owner of image")
	}
	if !ok {
		return http.NewResponse([]byte("lacking ownership of specified image"), http.StatusForbidden), nil
	}

	ok, err = h.database.DoesUserExist(receiver)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check if receiver exists")
	}
	if !ok {
		return http.NewResponse([]byte("receiver user does not exist"), http.StatusUnprocessableEntity), nil
	}

	ok, err = h.database.RequestImageTransfer(user.Username, receiver, imageID)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't request image transfer")
	}
	if ok {
		return http.NewResponse([]byte(
			"image transfer request created successfully",
		), http.StatusOK), nil
	} else {
		return http.NewResponse([]byte(
			"failed to request image transfer (likely because of ownership change of image)",
		), http.StatusUnprocessableEntity), nil
	}

}

func (h *transferHandler) serveHTTPGet(ctx http.Context, _ *http.Request) (http.Response, error) {
	captcha, err := http.GenerateCaptcha(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't generate captcha")
	}
	return http.NewResponse([]byte(
		fmt.Sprintf(`
<form method="post">
	<label for="image_id">Image ID:</label>
	<input type="text" id="image_id" name="%s"><br>
	<label for="receiver">Receiver username:</label>
	<input type="text" id="receiver" name="%s"><br>
	<img src="%s" alt="here be captcha"><br>
	<label for="captcha">Captcha:</label>
	<input type="text" id="captcha" name="%s"><br>
	<input type="submit" value="Transfer">
</form>
`,
			transferImageIDField,
			transferReceiverField,
			captcha,
			transferCaptchaField,
		)), http.StatusOK), nil
}
