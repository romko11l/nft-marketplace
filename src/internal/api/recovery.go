package api

import (
	"fmt"
	"strconv"

	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

const (
	recoveryImageIDField     = "image_id"
	recoveryImageSecretField = "image_secret"
	recoveryCaptchaField     = "captcha"
)

type recoveryDatabase interface {
	GetImageSecretHash(id int) (string, error)
	RequestRecoverImageOwnership(id int, username, secretHash string) error
}

func NewRecoveryHandler(
	database recoveryDatabase,
	hashChecker hashChecker,
) *recoveryHandler {
	return &recoveryHandler{
		database:    database,
		hashChecker: hashChecker,
	}
}

type recoveryHandler struct {
	database    recoveryDatabase
	hashChecker hashChecker
}

func (h *recoveryHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("authorization required"), http.StatusUnauthorized), nil
	}
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *recoveryHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm

	captcha := form.Get(recoveryCaptchaField)
	ok, err := http.ValidateCaptcha(ctx, captcha)

	if !ok {
		return nil, errors.Error("couldn't validate captcha")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	imageIDString := form.Get(recoveryImageIDField)
	imageID, err := strconv.Atoi(imageIDString)
	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "couldn't parse image id").Error()), http.StatusUnprocessableEntity), nil
	}
	imageSecret := form.Get(recoveryImageSecretField)

	imageSecretHash, err := h.database.GetImageSecretHash(imageID)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't retrieve image secret hash")
	}

	if imageSecretHash == "" {
		return http.NewResponse([]byte("no such image"), http.StatusUnprocessableEntity), nil
	}

	secretValid, err := h.hashChecker.CheckHashed(imageSecretHash, imageSecret)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check image secret validity")
	}
	if !secretValid {
		return http.NewResponse([]byte("invalid secret provided"), http.StatusUnprocessableEntity), nil
	}

	user, _ := ctx.GetUser()
	err = h.database.RequestRecoverImageOwnership(imageID, user.Username, imageSecretHash)

	if err != nil {
		return nil, errors.Wrap(err, "couldn't request image ownership recovery")
	}

	return http.NewResponse([]byte(
		"image recovery requested successfully",
	), http.StatusOK), nil
}

func (h *recoveryHandler) serveHTTPGet(ctx http.Context, _ *http.Request) (http.Response, error) {
	captcha, err := http.GenerateCaptcha(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't generate captcha")
	}
	return http.NewResponse([]byte(
		fmt.Sprintf(`
<form method="post">
	<label for="image_id">Image ID:</label>
	<input type="text" id="image_id" name="%s"><br>
	<label for="secret">Image secret:</label>
	<input type="password" id="secret" name="%s"><br>
	<img src="%s" alt="here be captcha"><br>
	<label for="captcha">Captcha:</label>
	<input type="text" id="captcha" name="%s"><br>
	<input type="submit" value="Recover">
</form>
`,
			recoveryImageIDField,
			recoveryImageSecretField,
			captcha,
			recoveryCaptchaField,
		)), http.StatusOK), nil
}
