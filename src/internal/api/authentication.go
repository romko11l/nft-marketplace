package api

import (
	"fmt"

	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

const (
	authenticationUsernameField = "username"
	authenticationPasswordField = "password"
	authenticationCaptchaField  = "captcha"
)

type authenticationDatabase interface {
	DoesUserExist(username string) (bool, error)
	IsUserModerator(username string) (bool, error)
	GetUserPasswordHash(username string) (string, error)
	GetUserSessionKey(username string) (string, error)
}

type hashChecker interface {
	CheckHashed(hashed, raw string) (bool, error)
}

func NewAuthenticationHandler(
	database authenticationDatabase,
	hashChecker hashChecker,
) *authenticationHandler {
	return &authenticationHandler{
		database:    database,
		hashChecker: hashChecker,
	}
}

type authenticationHandler struct {
	database    authenticationDatabase
	hashChecker hashChecker
}

func (h *authenticationHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *authenticationHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm
	username := form.Get(authenticationUsernameField)

	password := form.Get(authenticationPasswordField)

	captcha := form.Get(authenticationCaptchaField)
	ok, err := http.ValidateCaptcha(ctx, captcha)
	if !ok {
		return nil, errors.Wrap(err, "couldn't validate captcha")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	userExists, err := h.database.DoesUserExist(username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check if user exists")
	}
	if !userExists {
		return http.NewResponse([]byte("wrong username and/or password"), http.StatusUnprocessableEntity), nil
	}

	passwordHash, err := h.database.GetUserPasswordHash(username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't retrieve user password hash")
	}

	passwordMatches, err := h.hashChecker.CheckHashed(passwordHash, password)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check if hashed password matches provided one")
	}
	if !passwordMatches {
		return http.NewResponse([]byte("wrong username and/or password"), http.StatusUnprocessableEntity), nil
	}

	sessionKey, err := h.database.GetUserSessionKey(username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't retrieve user session key")
	}

	isModerator, err := h.database.IsUserModerator(username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't determine if user is moderator")
	}

	session := ctx.GetSession()
	session.Values[http.AuthInfoKey] = http.AuthInfo{
		Username:    username,
		SessionKey:  sessionKey,
		IsModerator: isModerator,
	}
	ctx.UpdateSession(session)

	return http.NewResponse([]byte("logged in successfully"), http.StatusOK), nil
}

func (h *authenticationHandler) serveHTTPGet(ctx http.Context, _ *http.Request) (http.Response, error) {
	captcha, err := http.GenerateCaptcha(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't generate captcha")
	}
	return http.NewResponse([]byte(
		fmt.Sprintf(`
<form method="post">
	<label for="username">Username:</label>
	<input type="text" id="username" name="%s"><br>
	<label for="password">Password:</label>
	<input type="password" id="password" name="%s"><br>
	<img src="%s" alt="here be captcha"><br>
	<label for="captcha">Captcha:</label>
	<input type="text" id="captcha" name="%s"><br>
	<input type="submit" value="Login">
</form>
`,
			authenticationUsernameField,
			authenticationPasswordField,
			captcha,
			authenticationCaptchaField,
		)), http.StatusOK), nil
}
