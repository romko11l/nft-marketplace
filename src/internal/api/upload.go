package api

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/url"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

// 10 << 20 specifies a maximum upload of 10 MB files.
const maxImageSize = 10 << 20

type imageHasher interface {
	Hash([]byte) (string, error)
}

func saveImage(
	body []byte,
	user dto.User,
	isPrivate string,
	imageHasher imageHasher,
	dp *database.DataProvider,
) (http.Response, error) {
	imageHash, err := imageHasher.Hash(body)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	imageID, key, err := dp.SaveImage(body, user.Username, imageHash, isPrivate == "private")
	if err != nil {
		return http.NewResponse([]byte("The image is already on the server"), http.StatusInternalServerError), nil
	}

	return http.NewResponse([]byte(fmt.Sprintf("The image was uploaded successfully<br>Image ID: %d<br>Your secret key: %s", imageID, key)), http.StatusOK), nil
}

func NewUploadHandler(
	imageHasher imageHasher,
	db *database.DataProvider,
) *uploadHandler {
	return &uploadHandler{
		DataProvider: db,
		ImageHasher:  imageHasher,
	}
}

type uploadHandler struct {
	ImageHasher  imageHasher
	DataProvider *database.DataProvider
}

func (h uploadHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	if request.Method != http.MethodPost {
		return h.ServeHTTPGet(ctx, request)
	}

	err := request.ParseMultipartForm(maxImageSize)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	captcha := request.FormValue("captcha")
	ok, err = http.ValidateCaptcha(ctx, captcha)

	if !ok {
		return nil, errors.Error("couldn't validate captcha")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	file, _, err := request.FormFile("img")
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	isPrivate := request.FormValue("isPrivate")

	resp, err := saveImage(
		fileBytes,
		user,
		isPrivate,
		h.ImageHasher,
		h.DataProvider,
	)

	return resp, err
}

func (h uploadHandler) ServeHTTPGet(ctx http.Context, request *http.Request) (http.Response, error) {
	captcha, err := http.GenerateCaptcha(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't generate captcha")
	}

	resp := fmt.Sprintf(`<font size="+2">Upload image from local storage</font>
	<form action="/upload" method="POST" enctype="multipart/form-data">
		  Select image to upload:<br>
		  <input type="file" name="img"><br><br>
		  <input type="checkbox" name="isPrivate" value="private" checked>Private<br>
		  <img src="%[1]s" alt="here be captcha"><br>
		  <label for="captcha">Captcha:</label>
		  <input type="text" id="captcha" name="captcha"><br>
		  <input type="submit" value="Upload Image" name="submit">
	</form>
	<br><br>
	<font size="+2">Upload image via url</font>
	<form action="/upload_from_url" method="POST">
		  Enter URL to upload:<br>
		  <input type="text" name="download_url"><br><br>
		<input type="checkbox" name="isPrivate" value="private" checked>Private<br>
		  <img src="%[1]s" alt="here be captcha"><br>
		  <label for="captcha">Captcha:</label>
		  <input type="text" id="captcha" name="captcha"><br>
		  <input type="submit" value="Upload from URL" name="submit">
	</form>`, captcha)

	return http.NewResponse([]byte(resp), http.StatusOK), nil
}

func NewUploadFromURLHandler(
	imageHasher imageHasher,
	db *database.DataProvider,
) *uploadFromURLHandler {
	return &uploadFromURLHandler{
		DataProvider: db,
		ImageHasher:  imageHasher,
	}
}

type uploadFromURLHandler struct {
	ImageHasher  imageHasher
	DataProvider *database.DataProvider
}

func (h uploadFromURLHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	if request.Method != http.MethodPost {
		return http.NewResponse([]byte("Method not allowed"), http.StatusMethodNotAllowed), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm

	captcha := form.Get("captcha")
	ok, err = http.ValidateCaptcha(ctx, captcha)
	if !ok {
		return nil, errors.Error("couldn't validate captcha")
	}
	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	downloadURL := form.Get("download_url")
	isPrivate := form.Get("isPrivate")

	return h.downloadImageFromURL(downloadURL, user, isPrivate)
}

func (h uploadFromURLHandler) downloadImageFromURL(rawURL string, user dto.User, isPrivate string) (http.Response, error) {
	url, err := url.Parse(rawURL)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	client := http.NewSafeClient()

	req, err := http.NewRequest(
		http.MethodGet, url.String(),
	)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	imageResp, err := client.Do(req)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}
	defer imageResp.Body.Close()

	body, err := ioutil.ReadAll(io.LimitReader(imageResp.Body, maxImageSize))
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	resp, err := saveImage(
		body,
		user,
		isPrivate,
		h.ImageHasher,
		h.DataProvider,
	)

	return resp, err
}
