package api

import (
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewIndexHandler() indexHandler {
	return indexHandler{}
}

type indexHandler struct{}

func (h indexHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	ret := ""
	user, ok := ctx.GetUser()
	if !ok {
		ret += `
			<form action="/login">
				<input type="submit" class="mainbtn" value="Login" />
			</form>
			<form action="/register">
				<input type="submit" class="mainbtn" value="Register" />
			</form>
		`
	} else {
		ret += `
			<form action="/home">
				<input type="submit" class="mainbtn" value="Home" />
			</form>
			<form action="/users_info">
				<input type="submit" class="mainbtn" value="UsersInfo" />
			</form>
			<form action="/feed">
				<input type="submit" class="mainbtn" value="MyFeed" />
			</form>
			<form action="/upload">
				<input type="submit" class="mainbtn" value="Upload Image" />
			</form>
			<form action="/recover">
				<input type="submit" class="mainbtn" value="Recover Image" />
			</form>
			<form action="/transfer">
				<input type="submit" class="mainbtn" value="Transfer Image" />
			</form>
			<form action="/transfer_approval">
				<input type="submit" class="mainbtn" value="Approve Or Deny Incoming Image Transfers" />
			</form>
			<form action="/my_images">
				<input type="submit" class="mainbtn" value="My Images" />
			</form>
		`
		if user.IsModerator {
			ret += `
			<form action="/recovery_approval">
				<input type="submit" class="mainbtn" value="Image Recovery Approval" />
			</form>
			<form action="/comment_complaint_approval">
				<input type="submit" class="mainbtn" value="Comments Complaints" />
			</form>
			<form action="/images_complaints">
				<input type="submit" class="mainbtn" value="Images Complaints" />
			</form>
			`
		}

		ret += `
			<form action="/logout">
				<input type="submit" class="mainbtn" value="Logout" />
			</form>
		`
	}
	return http.NewResponse([]byte(ret), http.StatusOK), nil
}
