package api

import (
	"bytes"
	"encoding/base64"
	"html/template"
	"strconv"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewImageHandler(db *database.DataProvider) *imageHandler {
	return &imageHandler{
		dataProvider: db,
	}
}

type imageHandler struct {
	dataProvider *database.DataProvider
}

//nolint:funlen
func (h imageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	imageIDstr := request.FormValue("id")
	if imageIDstr == "" {
		return http.NewResponse([]byte("404, Not Found"), http.StatusNotFound), nil
	}

	imageID, err := strconv.Atoi(imageIDstr)
	if err != nil {
		return nil, err
	}

	imageInfo, err := h.dataProvider.GetImageInfo(imageID)
	if err != nil {
		return http.NewResponse([]byte("404, Not Found"), http.StatusNotFound), nil
	}

	if imageInfo.IsPrivate {
		if user.Username != imageInfo.Owners[len(imageInfo.Owners)-1].Username {
			return http.NewResponse([]byte("403, Forbidden"), http.StatusForbidden), nil
		}
	}

	pageTemplate, err := template.ParseFiles("static/image.html")
	if err != nil {
		return nil, err
	}

	imageOwners := make([]map[string]interface{}, 0, len(imageInfo.Owners))
	for _, owner := range imageInfo.Owners {
		owner := owner
		imageOwners = append(imageOwners, map[string]interface{}{
			"username":           owner.Username,
			"ownershipTimestamp": owner.StartOwnership.Format("2006-01-02 15:04:05"),
		})
	}

	isOwner, err := h.dataProvider.IsUserOwnerOfImage(user.Username, imageInfo.ID)
	if err != nil {
		return nil, err
	}

	imageComments, err := h.dataProvider.GetComments(imageID)
	if err != nil {
		return nil, err
	}

	imageCommentsForTemplate := make([]map[string]interface{}, 0, len(imageComments))
	for _, comment := range imageComments {
		comment := comment
		//nolint:gosec // comment.Text is sanitized
		imageCommentsForTemplate = append(imageCommentsForTemplate, map[string]interface{}{
			"author":        comment.Author,
			"publishTime":   comment.PublishTime,
			"sanitizedText": template.HTML(comment.Text),
			"globalID":      comment.GlobalID,
		})
	}

	templateData := map[string]interface{}{
		"image": map[string]interface{}{
			"data":            base64.StdEncoding.EncodeToString(imageInfo.Body),
			"publicationTime": imageInfo.Owners[len(imageInfo.Owners)-1].StartOwnership.Format("2006-01-02 15:04:05"),
			"id":              imageInfo.ID,
			"owners":          imageOwners,
			"likes":           imageInfo.Likes,
			"isShownToOwner":  isOwner,
			"isPrivate":       imageInfo.IsPrivate,
			"comments":        imageCommentsForTemplate,
		},
		csrf.TemplateTag: csrf.TemplateField(request),
	}

	body := new(bytes.Buffer)
	err = pageTemplate.Execute(body, templateData)
	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}

func NewChangePrivacyHandler(db *database.DataProvider) changePrivacyHandler {
	return changePrivacyHandler{
		dataProvider: db,
	}
}

type changePrivacyHandler struct {
	dataProvider *database.DataProvider
}

func (h changePrivacyHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	imageIDstr := request.FormValue("image_id")
	if imageIDstr == "" {
		return http.NewResponse([]byte("404, Not Found"), http.StatusNotFound), nil
	}

	imageID, err := strconv.Atoi(imageIDstr)
	if err != nil {
		return nil, err
	}

	ok, err = h.dataProvider.ChangeImagePrivacy(user, imageID)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	if !ok {
		return http.NewResponse([]byte("403, Forbidden"), http.StatusForbidden), nil
	}

	return http.NewResponse([]byte("Image privacy changed successfully"), http.StatusOK), nil
}

func NewCommentComplaintHandler(db *database.DataProvider) commentComplaintHandler {
	return commentComplaintHandler{
		dataProvider: db,
	}
}

type commentComplaintHandler struct {
	dataProvider *database.DataProvider
}

func (h commentComplaintHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	commentIDstr := request.FormValue("global_comment_id")
	if commentIDstr == "" {
		return http.NewResponse([]byte("404, Not Found"), http.StatusNotFound), nil
	}

	commentID, err := strconv.Atoi(commentIDstr)
	if err != nil {
		return http.NewResponse([]byte("Server internal error"), http.StatusUnauthorized), err
	}

	err = h.dataProvider.RequestCommentComplaint(user, commentID)
	if err != nil {
		return http.NewResponse([]byte("Server internal error"), http.StatusUnauthorized), err
	}

	return http.NewResponse([]byte("Comment complaint posted"), http.StatusOK), nil
}
