package api

import (
	"bytes"
	"encoding/base64"
	"html/template"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

type userPageHandler struct {
	provider *database.DataProvider
}

func NewUserPageHandler(provider *database.DataProvider) userPageHandler {
	return userPageHandler{provider: provider}
}

func (h userPageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	query := request.URL.Query()
	username := query.Get("user")
	if username == "" {
		return http.NewResponse([]byte(`No "user" param provided`), http.StatusUnprocessableEntity), nil
	}

	template, err := template.ParseFiles("static/userinfo.html")
	if err != nil {
		return nil, err
	}

	description, err := h.provider.GetUserDescription(username)
	if err != nil {
		return http.NewResponse([]byte("No such user"), http.StatusNotFound), nil
	}

	avatar, avatarPresent, err := h.provider.GetUserAvatar(dto.User{Username: username})
	if err != nil {
		return nil, err
	}
	avatarData := ""
	if avatarPresent {
		avatarData = base64.StdEncoding.EncodeToString(avatar.Body)
	}

	templateData := map[string]interface{}{
		"user": map[string]interface{}{
			"username":    username,
			"description": description,
			"avatar": map[string]interface{}{
				"isPresent": avatarPresent,
				"data":      avatarData,
			},
		},
		"viewer": map[string]interface{}{
			"isModerator": user.IsModerator,
		},
		csrf.TemplateTag: csrf.TemplateField(request),
	}

	body := new(bytes.Buffer)
	err = template.Execute(body, templateData)
	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}

type homePageHandler struct {
	provider *database.DataProvider
}

func NewHomePageHandler(provider *database.DataProvider) homePageHandler {
	return homePageHandler{provider: provider}
}

func (h homePageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	switch request.Method {
	case http.MethodPost:
		err := request.ParseForm()
		if err != nil {
			return nil, err
		}

		if description, ok := request.Form["description"]; ok {
			err = h.provider.SetUserDescription(user.Username, description[0])
			if err != nil {
				return nil, err
			}
		}
		fallthrough
	case http.MethodGet:
		template, err := template.ParseFiles("static/home.html")
		if err != nil {
			return nil, err
		}

		description, err := h.provider.GetUserDescription(user.Username)
		if err != nil {
			return nil, err
		}

		avatar, avatarPresent, err := h.provider.GetUserAvatar(dto.User{Username: user.Username})
		if err != nil {
			return nil, err
		}
		avatarData := ""
		if avatarPresent {
			avatarData = base64.StdEncoding.EncodeToString(avatar.Body)
		}

		templateData := map[string]interface{}{
			"user": map[string]interface{}{
				"username":    user.Username,
				"description": description,
				"avatar": map[string]interface{}{
					"isPresent": avatarPresent,
					"data":      avatarData,
				},
			},
			csrf.TemplateTag: csrf.TemplateField(request),
		}

		body := new(bytes.Buffer)
		err = template.Execute(body, templateData)
		if err != nil {
			return nil, err
		}

		return http.NewResponse(body.Bytes(), http.StatusOK), nil
	}
	return nil, errors.Errorf("unsupported request method %s", request.Method)
}

type usersInfoHandler struct {
	provider *database.DataProvider
}

func NewUsersInfoHandler(provider *database.DataProvider) usersInfoHandler {
	return usersInfoHandler{provider: provider}
}

func (h usersInfoHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	return http.NewResponse([]byte(`
	<form method="get" action="/user">
		<label for="username">Username:</label>
		<input type="text" id="username" name="user"><br>
	</form>`), http.StatusOK), nil
}
