package api

import (
	"bytes"
	"fmt"
	"html/template"
	"strconv"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewComplainImageHandler(dataProvider *database.DataProvider) *complainImageHandler {
	return &complainImageHandler{dataProvider}
}

type complainImageHandler struct {
	dataProvider *database.DataProvider
}

func (h complainImageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	imageIDstr := request.FormValue("image_id")
	if imageIDstr == "" {
		return nil, fmt.Errorf("unknown image ID")
	}

	imageID, err := strconv.Atoi(imageIDstr)
	if err != nil {
		return nil, err
	}

	isPrivate, err := h.dataProvider.IsImagePrivate(imageID)
	if err != nil {
		return nil, err
	}

	if isPrivate {
		return nil, fmt.Errorf("image is private")
	}

	err = h.dataProvider.SaveImageComplaint(user.Username, imageID)
	if err != nil {
		return nil, err
	}

	return http.NewResponse([]byte("Complaint sent successfully"), http.StatusOK), nil
}

func NewComplainImageApprovalHandler(dataProvider *database.DataProvider) *complainImageApprovalHandler {
	return &complainImageApprovalHandler{dataProvider}
}

type complainImageApprovalHandler struct {
	dataProvider *database.DataProvider
}

func (h complainImageApprovalHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	if !user.IsModerator {
		return http.NewResponse([]byte("Must be moderator to view this resource"), http.StatusForbidden), nil
	}

	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, fmt.Errorf("unsupported request method %s", request.Method)
	}
}

func (h complainImageApprovalHandler) serveHTTPGet(ctx http.Context, request *http.Request) (http.Response, error) {
	template, err := template.ParseFiles("static/images_complaints.html")
	if err != nil {
		return nil, err
	}

	complaints, err := h.dataProvider.GetImageComplaints()
	if err != nil {
		return nil, err
	}

	body := new(bytes.Buffer)
	err = template.Execute(body, map[string]interface{}{
		"complaints":     complaints,
		csrf.TemplateTag: csrf.TemplateField(request),
	})

	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}

func (h complainImageApprovalHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	complaintIDstr := request.FormValue("complaint_id")
	complaintID, err := strconv.Atoi(complaintIDstr)
	if err != nil {
		return nil, err
	}

	ret := ""

	switch request.FormValue("action") {
	case "Confirm":
		err = h.dataProvider.ApproveImageComplaint(complaintID)
		if err != nil {
			return nil, err
		}
		ret += "Image was successfully privated"
	case "Deny":
		err = h.dataProvider.DeleteImageComplaint(complaintID)
		if err != nil {
			return nil, err
		}
		ret += "Complaint was successfully deleted"
	}

	return http.NewResponse([]byte(ret), http.StatusOK), nil
}
