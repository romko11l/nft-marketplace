package api

import (
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

type pubRightsPageHandler struct {
	provider *database.DataProvider
}

func NewPubRightsPageHandler(provider *database.DataProvider) pubRightsPageHandler {
	return pubRightsPageHandler{provider: provider}
}

func (h pubRightsPageHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	currUser, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}
	query := request.URL.Query()
	if user, ok := query["u"]; ok {
		userName := user[0]
		if request.Method == http.MethodPost {
			ok, err := h.provider.ChangePublicationRights(currUser.Username, userName)
			if err != nil {
				return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), nil
			}

			if !ok {
				return http.NewResponse([]byte("You can't change publication rights for this user"), http.StatusOK), nil
			}

			return http.NewResponse([]byte("Publication rights changed"), http.StatusOK), nil
		}

		return http.NewResponse([]byte("Requred Post method"), http.StatusBadRequest), nil
	}

	return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), nil
}

func temp(s1, s2 string) {
}
