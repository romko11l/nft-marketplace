package api

import (
	"fmt"
	"regexp"

	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

const (
	registrationUsernameField = "username"
	registrationPasswordField = "password"
	registrationCaptchaField  = "captcha"
)

var (
	usernameRE = regexp.MustCompile(`^\w+$`)
)

type registrationDatabase interface {
	DoesUserExist(username string) (bool, error)
	IsUserModerator(username string) (bool, error)
	AddUser(username, passwordHash, sessionKey string) (bool, error)
}

type hasher interface {
	Hash(string) (string, error)
}

type sessionKeyGenerator interface {
	NewSessionKey() string
}

func NewRegistrationHandler(
	database registrationDatabase,
	hasher hasher,
	sessionKeyGenerator sessionKeyGenerator,
) *registrationHandler {
	return &registrationHandler{
		database:            database,
		hasher:              hasher,
		sessionKeyGenerator: sessionKeyGenerator,
	}
}

type registrationHandler struct {
	database            registrationDatabase
	hasher              hasher
	sessionKeyGenerator sessionKeyGenerator
}

func (h *registrationHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *registrationHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm

	captcha := form.Get(registrationCaptchaField)
	ok, err := http.ValidateCaptcha(ctx, captcha)

	if !ok {
		return nil, errors.Error("couldn't validate captcha")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "captcha validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	username := form.Get(registrationUsernameField)
	ok, err = h.validateUsername(username)

	if !ok {
		return nil, errors.Error("couldn't validate username")
	}

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "username validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	password := form.Get(registrationPasswordField)
	err = h.validatePassword(password)

	if err != nil {
		return http.NewResponse([]byte(errors.Wrap(err, "password validation failed").Error()), http.StatusUnprocessableEntity), nil
	}

	passwordHashed, err := h.hasher.Hash(password)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't hash password")
	}

	sessionKey := h.sessionKeyGenerator.NewSessionKey()

	added, err := h.database.AddUser(username, passwordHashed, sessionKey)

	if err != nil {
		return nil, errors.Wrap(err, "couldn't add user")
	}

	if !added {
		return http.NewResponse([]byte(
			"couldn't register user, supposedly because another user registered with the same name at the same time. try again?",
		), http.StatusUnprocessableEntity), nil
	}

	isModerator, err := h.database.IsUserModerator(username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't check if user is moderator")
	}

	session := ctx.GetSession()
	session.Values[http.AuthInfoKey] = http.AuthInfo{
		Username:    username,
		SessionKey:  sessionKey,
		IsModerator: isModerator,
	}
	ctx.UpdateSession(session)

	return http.NewResponse([]byte(
		"registered successfully",
	), http.StatusOK), nil
}

func (h *registrationHandler) serveHTTPGet(ctx http.Context, _ *http.Request) (http.Response, error) {
	captcha, err := http.GenerateCaptcha(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't generate captcha")
	}
	return http.NewResponse([]byte(
		fmt.Sprintf(`
<form method="post">
	<label for="username">Username:</label>
	<input type="text" id="username" name="%s"><br>
	<label for="password">Password:</label>
	<input type="password" id="password" name="%s"><br>
	<img src="%s" alt="here be captcha"><br>
	<label for="captcha">Captcha:</label>
	<input type="text" id="captcha" name="%s"><br>
	<input type="submit" value="Register">
</form>
`,
			registrationUsernameField,
			registrationPasswordField,
			captcha,
			registrationCaptchaField,
		)), http.StatusOK), nil
}

func (h *registrationHandler) validateUsername(username string) (bool, error) {
	if !usernameRE.MatchString(username) {
		return true, errors.Error("username must consist of letters, digits and underscores")
	}
	exists, err := h.database.DoesUserExist(username)
	if err != nil {
		return false, errors.Wrap(err, "couldn't check if user exists")
	}
	if exists {
		return true, errors.Error("username already taken")
	}
	return true, nil
}

func (h *registrationHandler) validatePassword(password string) error {
	const minPasswordLen = 15
	if len(password) < minPasswordLen {
		return errors.Errorf("password too short, must be at least %d characters long", minPasswordLen)
	}

	return nil
}
