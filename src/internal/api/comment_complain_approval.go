package api

import (
	"bytes"
	"html/template"
	"strconv"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewCommentComplaintApprovalHandler(database *database.DataProvider) *commentComplaintApprovalHandler {
	return &commentComplaintApprovalHandler{
		database: database,
	}
}

type commentComplaintApprovalHandler struct {
	database *database.DataProvider
}

func (h *commentComplaintApprovalHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}
	if !user.IsModerator {
		return http.NewResponse([]byte("Must be moderator to view this resource"), http.StatusForbidden), nil
	}
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *commentComplaintApprovalHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm
	commentIDString := form.Get("comment_id")
	commentID, err := strconv.Atoi(commentIDString)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse imageID")
	}

	action := form.Get("action")
	ret := ""
	switch action {
	case "delete":
		err := h.database.DeleteCommentAndComplaints(commentID)
		if err != nil {
			return nil, errors.Wrap(err, "deleting comment failed")
		}
		ret = "Comment deleted"
	case "deny":
		err := h.database.DenyCommentComplaints(commentID)
		if err != nil {
			return nil, errors.Wrap(err, "request deny failed")
		}
		ret = "Comment complaint denied successfully"

	}
	return http.NewResponse([]byte(
		ret,
	), http.StatusOK), nil
}

func (h *commentComplaintApprovalHandler) serveHTTPGet(ctx http.Context, request *http.Request) (http.Response, error) {

	template, err := template.ParseFiles("static/comment_complaint_approval.html")
	if err != nil {
		return nil, err
	}

	complaints, err := h.database.GetCommentsComplaints()
	if err != nil {
		return nil, err
	}

	body := new(bytes.Buffer)
	err = template.Execute(body, map[string]interface{}{
		"complaints":     complaints,
		csrf.TemplateTag: csrf.TemplateField(request),
	})
	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}
