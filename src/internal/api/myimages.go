package api

import (
	"bytes"
	"html/template"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/imageencoder"
)

func NewMyImagesHandler(dataProvider *database.DataProvider) myImagesHandler {
	return myImagesHandler{dataProvider}
}

type myImagesHandler struct {
	dataProvider *database.DataProvider
}

func (h myImagesHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	t, err := template.ParseFiles("static/my_images.html")
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	myImages, err := h.dataProvider.GetUsersImages(user)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	b64EncodedImgs := make([]imageencoder.EncodedPost, 0, len(myImages))

	for _, imgPost := range myImages {
		imgPost := imgPost
		b64EncodedImgs = append(b64EncodedImgs, imageencoder.NewEncodedPost(imgPost))
	}

	body := new(bytes.Buffer)
	err = t.Execute(body, b64EncodedImgs)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}
