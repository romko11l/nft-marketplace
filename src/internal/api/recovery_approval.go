package api

import (
	"bytes"
	"html/template"
	"strconv"

	"github.com/gorilla/csrf"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

type recoveryApprovalDatabase interface {
	GetImageRecoveryRequests() ([]dto.ImageRecoveryRequest, error)
	RequestRecoverImageOwnership(id int, username, secretHash string) error
	ApproveImageRecoveryRequest(imageID int, username string) (bool, error)
	DenyImageRecoveryRequest(imageID int, username string) error
}

func NewRecoveryApprovalHandler(
	database recoveryApprovalDatabase,
	hashChecker hashChecker,
) *recoveryApprovalHandler {
	return &recoveryApprovalHandler{
		database:    database,
		hashChecker: hashChecker,
	}
}

type recoveryApprovalHandler struct {
	database    recoveryApprovalDatabase
	hashChecker hashChecker
}

func (h *recoveryApprovalHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}
	if !user.IsModerator {
		return http.NewResponse([]byte("Must be moderator to view this resource"), http.StatusForbidden), nil
	}
	switch request.Method {
	case http.MethodPost:
		return h.serveHTTPPost(ctx, request)
	case http.MethodGet:
		return h.serveHTTPGet(ctx, request)
	default:
		return nil, errors.Errorf("unsupported request method %s", request.Method)
	}
}

func (h *recoveryApprovalHandler) serveHTTPPost(ctx http.Context, request *http.Request) (http.Response, error) {
	err := request.ParseForm()
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse form")
	}

	form := request.PostForm
	username := form.Get("username")
	imageIDString := form.Get("imageID")
	imageID, err := strconv.Atoi(imageIDString)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't parse imageID")
	}

	action := form.Get("action")
	ret := ""
	switch action {
	case "approve":
		ok, err := h.database.ApproveImageRecoveryRequest(imageID, username)
		if err != nil {
			return nil, errors.Wrap(err, "request approval failed")
		}
		if ok {
			ret = "Approved successfully"
		} else {
			ret = "Approval failed (likely due to image secret changing since request creation)"
		}
	case "deny":
		err := h.database.DenyImageRecoveryRequest(imageID, username)
		if err != nil {
			return nil, errors.Wrap(err, "request deny failed")
		}
		ret = "Recovery request denied successfully"

	}
	return http.NewResponse([]byte(
		ret,
	), http.StatusOK), nil
}

func (h *recoveryApprovalHandler) serveHTTPGet(ctx http.Context, request *http.Request) (http.Response, error) {

	template, err := template.ParseFiles("static/recovery_approval.html")
	if err != nil {
		return nil, err
	}

	requests, err := h.database.GetImageRecoveryRequests()
	if err != nil {
		return nil, err
	}

	body := new(bytes.Buffer)
	err = template.Execute(body, map[string]interface{}{
		"requests":       requests,
		csrf.TemplateTag: csrf.TemplateField(request),
	})
	if err != nil {
		return nil, err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}
