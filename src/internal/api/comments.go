package api

import (
	"fmt"
	"strconv"
	"time"

	"github.com/microcosm-cc/bluemonday"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewCommentsHandler(dataProvider *database.DataProvider) *commentsHandler {
	return &commentsHandler{dataProvider}
}

type commentsHandler struct {
	dataProvider *database.DataProvider
}

func (h commentsHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	user, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	err := request.ParseForm()
	if err != nil {
		return nil, err
	}

	textComment := request.FormValue("comments")

	imageIDstr := request.FormValue("image_id")
	if imageIDstr == "" {
		return nil, fmt.Errorf("unknown image ID")
	}

	imageID, err := strconv.Atoi(imageIDstr)
	if err != nil {
		return nil, err
	}

	p := bluemonday.UGCPolicy()

	sanitizeComment := p.Sanitize(textComment)

	if sanitizeComment == "" {
		return nil, fmt.Errorf("empty comment body")
	}

	comment := dto.Comment{
		Author:      user.Username,
		PublishTime: time.Now(),
		Text:        sanitizeComment,
	}

	err = h.dataProvider.AddComment(comment, imageID)
	if err != nil {
		return nil, err
	}

	return http.NewResponse([]byte("Comment sent successfully"), http.StatusOK), nil
}
