package api

import (
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
)

func NewLogoutHandler() logoutHandler {
	return logoutHandler{}
}

type logoutHandler struct{}

func (h logoutHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	session := ctx.GetSession()
	delete(session.Values, http.AuthInfoKey)
	ctx.UpdateSession(session)

	return http.NewResponse([]byte("Logged out successfully"), http.StatusOK), nil
}
