package api

import (
	"bytes"
	"html/template"

	"gitlab.com/romko11l/nft-marketplace/src/internal/database"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/http"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/imageencoder"
)

func NewImagesFeedHandler(dataProvider *database.DataProvider) imagesFeedHandler {
	return imagesFeedHandler{dataProvider}
}

type imagesFeedHandler struct {
	dataProvider *database.DataProvider
}

func (h imagesFeedHandler) ServeHTTP(ctx http.Context, request *http.Request) (http.Response, error) {
	_, ok := ctx.GetUser()
	if !ok {
		return http.NewResponse([]byte("Authorization required"), http.StatusUnauthorized), nil
	}

	t, err := template.ParseFiles("static/feed.html")
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	imageFeed, err := h.dataProvider.GetImagesFeed()
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	b64EncodedImgs := make([]imageencoder.EncodedPost, 0, len(imageFeed))

	for _, imgPost := range imageFeed {
		imgPost := imgPost
		b64EncodedImgs = append(b64EncodedImgs, imageencoder.NewEncodedPost(imgPost))
	}

	body := new(bytes.Buffer)
	err = t.Execute(body, b64EncodedImgs)
	if err != nil {
		return http.NewResponse([]byte("Internal Server Error"), http.StatusInternalServerError), err
	}

	return http.NewResponse(body.Bytes(), http.StatusOK), nil
}
