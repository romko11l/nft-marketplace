package http

type sessionKeysStorage interface {
	GetUserSessionKey(username string) (string, error)
	SetUserSessionKey(username, key string) error
}
