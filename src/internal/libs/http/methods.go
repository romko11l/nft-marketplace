package http

import (
	"net/http"
)

const (
	MethodPost = http.MethodPost
	MethodGet  = http.MethodGet
)
