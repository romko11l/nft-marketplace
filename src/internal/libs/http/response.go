package http

func NewResponse(body []byte, statusCode int) *response {
	return &response{
		body:       body,
		statusCode: statusCode,
	}
}

type response struct {
	body       []byte
	statusCode int
}

func (r *response) GetBody() []byte {
	return r.body
}

func (r *response) GetStatusCode() int {
	return r.statusCode
}
