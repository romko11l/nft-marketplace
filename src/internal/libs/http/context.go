package http

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
)

func NewBaseContext(writer http.ResponseWriter, request *Request, cookieStore *sessions.CookieStore) (*baseContext, error) {
	ret := &baseContext{
		writer:      writer,
		request:     request,
		cookieStore: cookieStore,
	}

	err := ret.initSession()
	return ret, err
}

type baseContext struct {
	writer      http.ResponseWriter
	request     *Request
	cookieStore *sessions.CookieStore
	session     *sessions.Session
}

func (c *baseContext) GetUser() (dto.User, bool) {
	return dto.User{}, false
}

func (c *baseContext) GetLogger() log.Logger {
	return log.NewLogger()
}

func (c *baseContext) initSession() error {
	const sessionCookieName = "session"
	var err error
	c.session, err = c.cookieStore.Get(c.request, sessionCookieName)
	return err
}

func (c *baseContext) GetSession() *sessions.Session {
	return c.session
}

func (c *baseContext) UpdateSession(session *sessions.Session) {
	c.session = session
}

func NewContextWithUser(ctx Context, user dto.User) *contextWithUser {
	return &contextWithUser{
		Context: ctx,
		user:    user,
	}
}

type contextWithUser struct {
	Context
	user dto.User
}

func (c *contextWithUser) GetUser() (dto.User, bool) {
	return c.user, true
}

func NewContextWithLogger(ctx Context, logger log.Logger) *contextWithLogger {
	return &contextWithLogger{
		Context: ctx,
		logger:  logger,
	}
}

type contextWithLogger struct {
	Context
	logger log.Logger
}

func (c *contextWithLogger) GetLogger() log.Logger {
	return c.logger
}
