package http

import (
	"fmt"
	"net"
	"net/http"
	"syscall"
	"time"
)

func safeResolveControl(network string, address string, conn syscall.RawConn) error {
	if !(network == "tcp4") {
		return fmt.Errorf("%s is not a safe network type", network)
	}

	host, _, err := net.SplitHostPort(address) // TODO: second val - port. Need to be filtred?
	if err != nil {
		return fmt.Errorf("%s is not a valid host/port pair: %s", address, err)
	}

	ipaddress := net.ParseIP(host)
	if ipaddress == nil {
		return fmt.Errorf("%s is not a valid IP address", host)
	}

	if ipaddress.IsPrivate() || ipaddress.IsLoopback() || ipaddress.IsUnspecified() {
		return fmt.Errorf("%s is private IP address", host)
	}

	return nil
}

func NewSafeClient() *http.Client {
	dialer := &net.Dialer{
		Timeout:   10 * time.Second,
		KeepAlive: 10 * time.Second,
		Control:   safeResolveControl,
	}

	transport := &http.Transport{
		DialContext:         dialer.DialContext,
		MaxIdleConns:        100,
		IdleConnTimeout:     10 * time.Second,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	return &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout:   10 * time.Second,
		Transport: transport,
	}
}
