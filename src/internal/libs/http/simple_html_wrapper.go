package http

import (
	"fmt"
)

func NewSimpleHTMLWrapper(
	handler Handler,
) *simpleHTMLWrapper {
	return &simpleHTMLWrapper{
		wrapped: handler,
	}
}

type simpleHTMLWrapper struct {
	wrapped Handler
}

func (w *simpleHTMLWrapper) ServeHTTP(ctx Context, request *Request) (Response, error) {
	response, err := w.wrapped.ServeHTTP(ctx, request)
	if err != nil || response == nil {
		return response, err
	}

	return NewResponse([]byte(fmt.Sprintf(`
	<!doctype html>
	<html>
	<head>
		<meta charset="utf-8">
		<title>NFT-Imageboard</title>
		<link rel="stylesheet" href="/static/css/style.css">
	</head>
	<body>
		<header>
			<a href="/">
			<h1 class="header-title">NFT-Imageboard</h1> </br>
			</a>
		</header>
%s
</body>
</html>
`, response.GetBody())), response.GetStatusCode()), nil

}
