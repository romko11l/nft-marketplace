package http

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
)

func NewContextWrapper(
	handler Handler,
	cookieStore *sessions.CookieStore,
	logger log.Logger,
) *contextWrapper {
	return &contextWrapper{
		wrapped:     handler,
		cookieStore: cookieStore,
		logger:      logger,
	}
}

type contextWrapper struct {
	wrapped     Handler
	cookieStore *sessions.CookieStore
	logger      log.Logger
}

func (w *contextWrapper) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	ctx, err := NewBaseContext(writer, request, w.cookieStore)
	if err != nil {
		w.logger.Error(errors.Wrap(err, "couldn't init session context").Error())
	}

	response, err := w.wrapped.ServeHTTP(NewContextWithLogger(ctx, w.logger), request)
	if err != nil {
		w.handleError(writer, err)
		return
	}

	err = ctx.GetSession().Save(request, writer)
	if err != nil {
		w.handleError(writer, errors.Wrap(err, "couldn't update session"))
		return
	}
	w.writeResponse(writer, response)
}

func (w *contextWrapper) handleError(writer http.ResponseWriter, err error) {
	w.logger.Error(err.Error())
	http.Error(writer, "internal server error", StatusInternalServerError)
}

func (w *contextWrapper) writeResponse(writer http.ResponseWriter, response Response) {
	writer.WriteHeader(response.GetStatusCode())
	_, err := writer.Write(response.GetBody())
	if err != nil {
		w.logger.Error(errors.Wrap(err, "couldn't write http response").Error())
	}
}
