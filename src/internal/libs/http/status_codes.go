package http

import "net/http"

const (
	StatusOK                  = http.StatusOK
	StatusInternalServerError = http.StatusInternalServerError
	StatusBadRequest          = http.StatusBadRequest
	StatusUnauthorized        = http.StatusUnauthorized
	StatusNotFound            = http.StatusNotFound
	StatusForbidden           = http.StatusForbidden
	StatusMethodNotAllowed    = http.StatusMethodNotAllowed
	StatusUnprocessableEntity = http.StatusUnprocessableEntity
)
