package http

import (
	"encoding/gob"

	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
)

const AuthInfoKey = "auth_info"

func NewAuthWrapper(
	handler Handler,
	sessionKeysStorage sessionKeysStorage,
) *authWrapper {
	return &authWrapper{
		wrapped:            handler,
		sessionKeysStorage: sessionKeysStorage,
	}
}

type authWrapper struct {
	wrapped            Handler
	sessionKeysStorage sessionKeysStorage
}

func (w *authWrapper) ServeHTTP(ctx Context, request *Request) (Response, error) {
	user, err := w.getUserFromSession(ctx)
	if err != nil {
		return nil, err
	}
	if user != nil {
		ctx = NewContextWithUser(ctx, *user)
	}
	return w.wrapped.ServeHTTP(ctx, request)
}

func (w *authWrapper) getUserFromSession(ctx Context) (*dto.User, error) {
	session := ctx.GetSession()
	authInfoInfo, ok := session.Values[AuthInfoKey]
	if !ok {
		return nil, nil
	}

	authInfoStructured, ok := authInfoInfo.(AuthInfo)
	if !ok {
		return nil, errors.Errorf("couldn't cast authInfoInfo to corresponding type: %#V", authInfoInfo)
	}

	userSessionKey, err := w.sessionKeysStorage.GetUserSessionKey(authInfoStructured.Username)
	if err != nil {
		return nil, errors.Wrap(err, "couldn't retrieve user session key from storage")
	}

	if userSessionKey == authInfoStructured.SessionKey {
		return &dto.User{
			Username:    authInfoStructured.Username,
			IsModerator: authInfoStructured.IsModerator,
		}, nil
	} else {
		delete(session.Values, AuthInfoKey)
		ctx.UpdateSession(session)
		return nil, nil
	}
}

type AuthInfo struct {
	Username    string
	IsModerator bool
	SessionKey  string
}

func init() {
	gob.Register(AuthInfo{})
}
