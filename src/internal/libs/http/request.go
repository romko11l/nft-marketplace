package http

import (
	"net/http"
	"net/url"
)

func NewRequest(method, rawURL string) (*http.Request, error) {
	url, err := url.Parse(rawURL)
	if err != nil {
		return nil, err
	}
	return &http.Request{
		Method: method,
		URL:    url,
	}, nil
}
