package http

import (
	"net/http"

	"github.com/gorilla/sessions"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
)

type Request = http.Request

type Response interface {
	GetBody() []byte
	GetStatusCode() int
}

type Context interface {
	GetUser() (dto.User, bool)
	GetLogger() log.Logger

	GetSession() *sessions.Session
	UpdateSession(*sessions.Session)
}

type Handler interface {
	ServeHTTP(Context, *Request) (Response, error)
}
