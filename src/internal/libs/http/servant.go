package http

import (
	"crypto/rand"
	"net/http"
	"time"

	"github.com/gorilla/csrf"
	"github.com/gorilla/sessions"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
)

type ServerConfig struct {
	Addr           string
	ReadTimeout    time.Duration
	WriteTimeout   time.Duration
	IdleTimeout    time.Duration
	MaxHeaderBytes int
}

type SessionStorageConfig struct {
	AuthenticationKey []byte
	EncryptionKey     []byte
}

type ServantConfig struct {
	Server         ServerConfig
	SessionStorage SessionStorageConfig
}

func NewServant(
	config ServantConfig,
	sessionKeysStorage sessionKeysStorage,
	logger log.Logger,
	handlers map[string]Handler,
) (*servant, error) {
	ret := &servant{
		server: &http.Server{
			Addr:           config.Server.Addr,
			ReadTimeout:    config.Server.ReadTimeout,
			WriteTimeout:   config.Server.WriteTimeout,
			IdleTimeout:    config.Server.IdleTimeout,
			MaxHeaderBytes: config.Server.MaxHeaderBytes,
		},
		logger: logger,
	}

	cookieStore := sessions.NewCookieStore(
		config.SessionStorage.AuthenticationKey,
		config.SessionStorage.EncryptionKey,
	)

	mux := http.NewServeMux()

	const csrfAuthKeyLen = 32
	csrfAuthKey := make([]byte, csrfAuthKeyLen)
	_, err := rand.Read(csrfAuthKey)
	if err != nil {
		logger.Fatalln(errors.Wrap(err, "couldn't generate random csrfAuthKey"))
	}

	csrfMiddleware := csrf.Protect(csrfAuthKey, csrf.Secure(false))

	for handle, handler := range handlers {
		handler = NewAuthWrapper(handler, sessionKeysStorage)
		if handle == "/home" || handle == "/change_privacy" || handle == "/image" || handle == "/set_avatar" || handle == "/recovery_approval" || handle == "/transfer_approval" || handle == "/send_comment" || handle == "/user" || handle == "/change_pub_rights" || handle == "/complaint_comment" || handle == "/comment_complaint_approval" || handle == "/complain_image" || handle == "images_complaints" {
			mux.Handle(handle, csrfMiddleware(NewContextWrapper(handler, cookieStore, logger)))
		} else {
			mux.Handle(handle, NewContextWrapper(handler, cookieStore, logger))
		}
	}

	fs := http.FileServer(http.Dir("static"))
	mux.Handle("/static/", http.StripPrefix("/static", fs))

	ret.server.Handler = mux

	return ret, nil
}

type servant struct {
	server *http.Server
	logger log.Logger
}

func (s *servant) Serve() error {
	return s.server.ListenAndServe()
}
