package http

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/steambap/captcha"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
)

const captchaCookieKey = "captcha"

func ValidateCaptcha(ctx Context, captcha string) (bool, error) {
	session := ctx.GetSession()
	expectedCaptcha, ok := session.Values[captchaCookieKey]
	if !ok {
		return false, errors.Error("couldn't retrieve expected captcha value")
	}
	delete(session.Values, captchaCookieKey)
	ctx.UpdateSession(session)
	expectedCaptchaString, ok := expectedCaptcha.(string)
	if !ok {
		return false, errors.Error("invalid expected captcha value")
	}
	if !strings.EqualFold(expectedCaptchaString, captcha) {
		return true, errors.Errorf("expected different captcha value")
	}
	return true, nil
}

func GenerateCaptcha(ctx Context) (string, error) {
	const dimX = 150
	const dimY = 50

	captcha, err := captcha.New(dimX, dimY)
	if err != nil {
		return "", err
	}

	session := ctx.GetSession()
	session.Values[captchaCookieKey] = captcha.Text
	ctx.UpdateSession(session)

	buffer := new(bytes.Buffer)
	err = captcha.WriteImage(buffer)
	if err != nil {
		return "", errors.Wrap(err, "couldn't encode image as png")
	}
	return fmt.Sprintf("data:image/png;base64,%s", base64.StdEncoding.EncodeToString(buffer.Bytes())), nil
}
