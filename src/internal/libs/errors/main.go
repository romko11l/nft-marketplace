package errors

import (
	"fmt"
	"strings"
)

func Wrap(err error, ctx string) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf("%s: %s", ctx, err.Error())
}

func Wrapf(err error, format string, args ...interface{}) error {
	if err == nil {
		return nil
	}
	return fmt.Errorf("%s: %s", fmt.Sprintf(format, args...), err.Error())
}

func Error(msg string) error {
	return fmt.Errorf("%s", msg)
}

func Errorf(format string, args ...interface{}) error {
	return fmt.Errorf(format, args...)
}

func Collapse(errors ...error) error {
	errorsFiltered := make([]error, 0, len(errors))
	for _, err := range errors {
		if err != nil {
			errorsFiltered = append(errorsFiltered, err)
		}
	}
	if len(errorsFiltered) == 0 {
		return nil
	}
	if len(errorsFiltered) == 1 {
		return errorsFiltered[0]
	}
	errorMessages := make([]string, 0, len(errorsFiltered))
	for _, err := range errorsFiltered {
		if err != nil {
			errorMessages = append(errorMessages, strings.Split(err.Error(), "\n")...)
		}
	}
	return Errorf("collapsed errors:\n\t%s", strings.Join(errorMessages, "\n\t"))
}
