package errors

func Recover(errptr *error) {
	recovered := recover()
	if recovered == nil {
		return
	}
	err, ok := recovered.(error)
	if !ok {
		err = Collapse(*errptr, Errorf("non-error panic recovered: %s", recovered))
	}
	*errptr = err
}
