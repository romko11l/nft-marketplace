package hash

import (
	"bytes"
	"image"

	"github.com/corona10/goimagehash"
)

func NewImageHasher() *imageHasher {
	return &imageHasher{}
}

type imageHasher struct{}

func (h *imageHasher) Hash(bs []byte) (string, error) {
	img, _, err := image.Decode(bytes.NewReader(bs))
	if err != nil {
		return "", err
	}

	hash, err := goimagehash.AverageHash(img)
	if err != nil {
		return "", err
	}

	return hash.ToString(), nil
}
