package hash

import (
	"github.com/matthewhartstonge/argon2"
)

func NewHasher() *hasher {
	return &hasher{
		config: argon2.DefaultConfig(),
	}
}

type hasher struct {
	config argon2.Config
}

func (h *hasher) Hash(what string) (string, error) {
	encoded, err := h.config.HashEncoded([]byte(what))
	if err != nil {
		return "", err
	}
	return string(encoded), nil
}

func (h *hasher) CheckHashed(hashed, raw string) (bool, error) {
	decodedHash, err := argon2.Decode([]byte(hashed))
	if err != nil {
		return false, err
	}
	return decodedHash.Verify([]byte(raw))
}
