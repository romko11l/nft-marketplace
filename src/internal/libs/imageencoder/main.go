package imageencoder

import (
	"encoding/base64"

	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
)

type EncodedPost struct {
	Image       string
	ImageID     int
	LikesAmount int
}

func NewEncodedPost(imagePost dto.ImagePost) EncodedPost {
	return EncodedPost{
		Image:       base64.StdEncoding.EncodeToString(imagePost.Image),
		ImageID:     imagePost.ImageID,
		LikesAmount: imagePost.LikesAmount,
	}
}
