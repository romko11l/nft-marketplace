package recovery_keys

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_dictionaryType_Has(t *testing.T) {
	testCases := []struct {
		name         string
		dictionary   dictionaryType
		key          string
		expectResult bool
	}{
		{
			name:         "empty dictionary",
			dictionary:   newDictionary(),
			key:          "some key",
			expectResult: false,
		},
		{
			name:         "nonempty dictionary, missing key",
			dictionary:   newDictionary("a", "b", "c"),
			key:          "d",
			expectResult: false,
		},
		{
			name:         "nonempty dictionary, existing key",
			dictionary:   newDictionary("a", "b", "c"),
			key:          "c",
			expectResult: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			assert.Equal(t, testCase.expectResult, testCase.dictionary.Has(testCase.key))
		})
	}
}

type IntnStub struct{}

func (s IntnStub) Intn(n int) int {
	return n - 1
}

type IntnStub2 struct{}

func (s IntnStub2) Intn(n int) int {
	return 0
}

func Test_dictionaryType_PickRandom(t *testing.T) {
	testCases := []struct {
		name         string
		dictionary   dictionaryType
		randomSource randomSource
		expectResult string
	}{
		{
			name:         "empty dictionary",
			dictionary:   newDictionary(),
			randomSource: IntnStub{},
			expectResult: "",
		},
		{
			name:         "nonempty dictionary",
			dictionary:   newDictionary("a", "b", "c"),
			randomSource: IntnStub{},
			expectResult: "c",
		},
		{
			name:         "nonempty dictionary alt",
			dictionary:   newDictionary("a", "b", "c"),
			randomSource: IntnStub2{},
			expectResult: "a",
		},
	}
	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			assert.Equal(t, testCase.expectResult, testCase.dictionary.PickRandom(testCase.randomSource))
		})
	}
}
