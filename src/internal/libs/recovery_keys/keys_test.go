package recovery_keys

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NewKey(t *testing.T) {
	testCases := []struct {
		name        string
		key         string
		expectError bool
	}{
		{
			name:        "valid format",
			key:         "q w e r t y u i o p [ ]",
			expectError: false,
		},
		{
			name:        "invalid format",
			key:         "abc def",
			expectError: true,
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			_, err := NewKey(testCase.key)
			if testCase.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
