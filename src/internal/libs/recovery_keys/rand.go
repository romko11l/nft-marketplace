package recovery_keys

import (
	cryptoRand "crypto/rand"
	"encoding/binary"
	"log"
	"math/rand"
)

//TODO: own wrapper with proper error handling

func NewSecureRand() *rand.Rand {
	return rand.New(secureRandSource{})
}

type secureRandSource struct{}

func (s secureRandSource) Seed(int64) {}

func (s secureRandSource) Int63() int64 {
	return int64(s.Uint64() & ^uint64(1<<63))
}

func (s secureRandSource) Uint64() uint64 {
	var ret uint64
	err := binary.Read(cryptoRand.Reader, binary.BigEndian, &ret)
	if err != nil {
		log.Fatalln(err) //TODO: proper error handling somehow
	}
	return ret
}
