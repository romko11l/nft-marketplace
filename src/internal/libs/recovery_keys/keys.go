package recovery_keys

import (
	"fmt"
	"strings"
)

const keyWords = 12
const keyDelimiter = " "

func NewKey(str string) (Key, error) {
	ret := Key(str)
	err := ret.checkFormat()
	if err != nil {
		return "", err
	}
	return ret, nil
}

func newRandomKey(wordsSource randomWordPicker, randomSource randomSource) (Key, error) {
	words := make([]string, 0, keyWords)
	for i := 0; i < keyWords; i++ {
		words = append(words, wordsSource.PickRandom(randomSource))
	}
	return NewKey(strings.Join(words, keyDelimiter))
}

type Key string

func (k Key) String() string {
	return string(k)
}

func (k Key) checkFormat() error {
	split := strings.Split(string(k), keyDelimiter)
	if len(split) != keyWords {
		return fmt.Errorf("key must be %d `%s`-separated words", keyWords, keyDelimiter)
	}
	return nil
}

type randomWordPicker interface {
	PickRandom(randomSource) string
}
