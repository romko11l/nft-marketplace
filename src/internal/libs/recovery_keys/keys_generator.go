package recovery_keys

func NewDefaultKeyGenerator() *keyGenerator {
	return &keyGenerator{
		wordsSource:  dictionary,
		randomSource: NewSecureRand(),
	}
}

type keyGenerator struct {
	wordsSource  randomWordPicker
	randomSource randomSource
}

func (g *keyGenerator) MakeKey() (string, error) {
	key, err := newRandomKey(g.wordsSource, g.randomSource)
	return key.String(), err
}
