package recovery_keys

func newDictionary(elements ...string) dictionaryType {
	set := make(map[string]struct{})
	for _, element := range elements {
		set[element] = struct{}{}
	}
	return dictionaryType{
		elementsSet: set,
		elements:    elements,
	}
}

type dictionaryType struct {
	elementsSet map[string]struct{}
	elements    []string
}

func (d dictionaryType) Has(element string) bool {
	_, ok := d.elementsSet[element]
	return ok
}

type randomSource interface {
	Intn(int) int
}

func (d dictionaryType) PickRandom(rand randomSource) string {
	if len(d.elements) == 0 {
		return ""
	}
	chosenWordIndex := rand.Intn(len(d.elements))
	return d.elements[chosenWordIndex]
}
