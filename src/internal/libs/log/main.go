package log

import (
	"fmt"
	stdlog "log"
	"os"
)

type logger struct {
	*stdlog.Logger
}

type Logger interface {
	Println(...interface{})
	Fatalln(...interface{})

	Printf(string, ...interface{})
	Fatalf(string, ...interface{})

	Error(string)

	Info(string)
	Infof(string, ...interface{})
}

func NewLogger() *logger {
	return &logger{
		Logger: stdlog.New(os.Stderr, "", stdlog.Ldate|stdlog.Ltime|stdlog.LUTC),
	}
}

func (l *logger) Info(msg string) {
	l.Printf("INFO: %s", msg)
}

func (l *logger) Infof(format string, args ...interface{}) {
	l.Printf("INFO: %s", fmt.Sprintf(format, args...))
}

func (l *logger) Error(msg string) {
	l.Printf("ERROR: %s\n", msg)
}
