package dto

import "time"

// ImagePost implements one image post from images
// feed that contains image and amount of people who
// liked this picture.
type ImagePost struct {
	Image       []byte
	ImageID     int
	LikesAmount int
}

// ImadeFeed implements images feed sorted by popularity.
// The more likes, the more popular.
type ImageFeed = []ImagePost // массив должен быть отсортирован по количество лайков (DESC)

// эти типы надо согласовать вместе, пока просто замокаю их, чтобы использовать в контракте
type Image struct {
	ID        int
	Hash      string
	Body      []byte
	Owners    []Owner  // дату публикации можно вытащить из нулевого owner-а
	Likes     []string // usernames
	IsPrivate bool
}

type User struct {
	Username    string
	IsModerator bool
}

type Owner struct {
	Username       string
	StartOwnership time.Time
}

type ImageRecoveryRequest struct {
	Username string
	ImageID  int
}

type ImageTransferRequest struct {
	SenderUsername   string
	ReceiverUsername string
	ImageID          int
}

type Comment struct {
	Author      string
	PublishTime time.Time
	Text        string
	GlobalID    int
}

type CommentComplaint struct {
	RequesterUsername string
	CommentID         int
	ImageID           int
	Text              string
	UserID            int
}

type ImageComplaint struct {
	ComplaintID int
	UserID      int
	ImageID     int
}
