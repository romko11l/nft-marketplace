package database

import (
	"context"
	"database/sql"
	"fmt"
	"sort"
	"time"

	_ "github.com/lib/pq"
	"gitlab.com/romko11l/nft-marketplace/src/internal/dto"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/errors"
	"gitlab.com/romko11l/nft-marketplace/src/internal/libs/log"
)

type hasher interface {
	Hash(string) (string, error)
}

type keyGenerator interface {
	MakeKey() (string, error)
}

// DataProvider supports connection to postgres and implements interfaces from API.
type DataProvider struct {
	connection   *sql.DB
	hasher       hasher
	keyGenerator keyGenerator
}

// Create new instance of DataProvider.
func NewDataProvider(connectionString string, hasher hasher, keyGenerator keyGenerator) (*DataProvider, error) {
	logger := log.NewLogger()

	connection, err := sql.Open("postgres", connectionString)
	if err != nil {
		return nil, err
	}
	err = connection.Ping()
	if err == nil {
		logger.Info("Database connector start with scheme: " + connectionString)
	}
	return &DataProvider{
		connection:   connection,
		hasher:       hasher,
		keyGenerator: keyGenerator,
	}, err
}

func (d *DataProvider) GetImagesFeed() (dto.ImageFeed, error) {
	rows, err := d.connection.Query("SELECT id, image FROM images WHERE is_private = false;")
	if err != nil {
		return nil, err
	}

	var id, likesAmount int
	var image []byte
	var posts []dto.ImagePost

	for rows.Next() {
		err = rows.Scan(&id, &image)
		if err != nil {
			return nil, err
		}

		row := d.connection.QueryRow("SELECT COUNT(*) FROM likes WHERE image_id=$1", id)
		err = row.Scan(&likesAmount)
		if err != nil {
			return nil, err
		}

		posts = append(posts, dto.ImagePost{Image: image, ImageID: id, LikesAmount: likesAmount})
	}
	err = rows.Close()

	sort.Slice(posts, func(i, j int) bool {
		return posts[i].LikesAmount > posts[j].LikesAmount
	})

	return posts, err
}

func (d *DataProvider) LikeImage(user dto.User, imageID int) (bool, error) {
	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}

	sqlStatement := "SELECT is_private FROM images WHERE id = $1;"

	var isPrivate bool
	row := tx.QueryRow(sqlStatement, imageID)

	err = row.Scan(&isPrivate)
	if err != nil {
		return false, err
	}

	if isPrivate {
		return false, nil
	}

	sqlStatement = "INSERT INTO likes (user_id, image_id) VALUES ((SELECT id FROM users WHERE username = $1), $2);"
	_, err = tx.ExecContext(ctx, sqlStatement, user.Username, imageID)
	if err != nil {
		error := tx.Rollback()
		if error != nil {
			return false, error
		}
		return false, err
	}

	return true, tx.Commit()
}

func (d *DataProvider) GetComments(imageID int) ([]dto.Comment, error) {
	sqlStatement := "SELECT username, comment_text, creation_time, comments.id FROM comments JOIN users ON users.id=author_id WHERE image_id=$1 ORDER BY creation_time DESC;"
	rows, err := d.connection.Query(sqlStatement, imageID)
	if err != nil {
		return nil, err
	}

	var comments []dto.Comment
	for rows.Next() {
		var comment dto.Comment
		err := rows.Scan(&comment.Author, &comment.Text, &comment.PublishTime, &comment.GlobalID)
		if err != nil {
			return nil, err
		}
		comments = append(comments, comment)
	}

	return comments, nil
}

func (d *DataProvider) AddComment(comment dto.Comment, imageID int) error {
	userID, err := d.GetUserID(comment.Author)
	if err != nil {
		return err
	}

	ctx := context.Background()

	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	sqlStatement := "SELECT is_private, owner_id FROM images WHERE id = $1;"

	var isPrivate bool
	var ownerID int
	row := tx.QueryRow(sqlStatement, imageID)

	err = row.Scan(&isPrivate, &ownerID)
	if err != nil {
		return err
	}
	if isPrivate && ownerID != userID {
		return errors.Collapse(errors.Error("Image is private"), tx.Rollback())
	}

	sqlStatement = "INSERT INTO comments (author_id, image_id, comment_text, creation_time) VALUES ($1, $2, $3, $4);"
	_, err = tx.ExecContext(ctx, sqlStatement, userID, imageID, comment.Text, comment.PublishTime)

	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return tx.Commit()
}

func (d *DataProvider) GetUserID(username string) (int, error) {
	var id int
	sqlStatement := "SELECT id FROM users WHERE username = $1;"
	row := d.connection.QueryRow(sqlStatement, username)
	err := row.Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (d *DataProvider) SaveImage(image []byte, username string, imageHash string, isPrivate bool) (int, string, error) {
	key, err := d.keyGenerator.MakeKey()
	if err != nil {
		return 0, "", err
	}

	keyHash, err := d.hasher.Hash(key)
	if err != nil {
		return 0, "", err
	}

	ownerID, err := d.GetUserID(username)
	if err != nil {
		return 0, "", err
	}

	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return 0, "", err
	}

	var publicationIsAllow bool
	sqlStatement := "SELECT publication_allowed FROM users WHERE username = $1;"
	row := tx.QueryRow(sqlStatement, username)
	err = row.Scan(&publicationIsAllow)
	if err != nil {
		return 0, "", errors.Collapse(err, tx.Rollback())
	}

	if !publicationIsAllow {
		return 0, "", errors.Collapse(errors.Error("Images upload not allowed for this user"), tx.Rollback())
	}

	sqlStatement = "INSERT INTO images (image, owner_id, secret, image_hash, is_private) VALUES ($1, $2, $3, $4, $5);"
	_, err = tx.ExecContext(ctx, sqlStatement, image, ownerID, keyHash, imageHash, isPrivate)
	if err != nil {
		error := tx.Rollback()
		if error != nil {
			return 0, "", error
		}
		return 0, "", err
	}

	sqlStatement = "INSERT INTO ownership_history (owner_id, image_id, time) VALUES ($1, (SELECT id FROM images WHERE image_hash = $2), $3);"
	_, err = tx.ExecContext(ctx, sqlStatement, ownerID, imageHash, time.Now())
	if err != nil {
		error := tx.Rollback()
		if error != nil {
			return 0, "", error
		}
		return 0, "", err
	}

	sqlStatement = "SELECT id FROM images WHERE image_hash=$1"
	row = tx.QueryRow(sqlStatement, imageHash)
	var id int
	err = row.Scan(&id)
	if err != nil {
		error := tx.Rollback()
		if error != nil {
			return 0, "", error
		}
		return 0, "", err
	}

	err = tx.Commit()
	return id, key, err
}

func (d *DataProvider) GetImageInfo(imageID int) (dto.Image, error) {
	var image dto.Image
	image.ID = imageID

	sqlStatement := "SELECT image_hash, image, is_private FROM images WHERE id=$1;"
	row := d.connection.QueryRow(sqlStatement, imageID)
	err := row.Scan(&image.Hash, &image.Body, &image.IsPrivate)
	if err != nil {
		return dto.Image{}, err
	}

	sqlStatement = "SELECT users.username FROM likes JOIN users ON users.id=likes.user_id WHERE likes.image_id=$1;"
	rows, err := d.connection.Query(sqlStatement, imageID)
	if err != nil {
		return dto.Image{}, err
	}
	for rows.Next() {
		var username string
		err = rows.Scan(&username)
		if err != nil {
			return dto.Image{}, err
		}
		image.Likes = append(image.Likes, username)
	}
	err = rows.Close()
	if err != nil {
		return image, err
	}

	sqlStatement = "SELECT users.username, ownership_history.time FROM ownership_history JOIN users ON users.id=ownership_history.owner_id WHERE ownership_history.image_id=$1 ORDER BY time;"
	rows, err = d.connection.Query(sqlStatement, imageID)
	if err != nil {
		return dto.Image{}, err
	}
	for rows.Next() {
		var owner dto.Owner
		err = rows.Scan(&owner.Username, &owner.StartOwnership)
		if err != nil {
			return dto.Image{}, err
		}
		image.Owners = append(image.Owners, owner)
	}
	err = rows.Close()
	if err != nil {
		return image, err
	}

	return image, nil
}

func (d *DataProvider) doesUserExist(tx *sql.Tx, username string) (bool, error) {
	var result bool
	row := tx.QueryRow("SELECT EXISTS(SELECT username FROM users WHERE username=$1);", username)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) DoesUserExist(username string) (bool, error) {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	ok, err := d.doesUserExist(tx, username)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	return ok, tx.Commit()
}

func (d *DataProvider) AddUser(username, password, sessionKey string) (bool, error) {
	sqlStatement := "INSERT INTO users (username, password_hash, session_key) VALUES ($1, $2, $3);"
	_, err := d.connection.Exec(sqlStatement, username, password, sessionKey)
	return true, err // ToDo: check existing user
}

func (d *DataProvider) GetUserPasswordHash(username string) (string, error) {
	var result string
	sqlStatement := "SELECT password_hash from users WHERE username=$1;"
	row := d.connection.QueryRow(sqlStatement, username)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) GetUserSessionKey(username string) (string, error) {
	var result string
	sqlStatement := "SELECT session_key from users WHERE username=$1;"
	row := d.connection.QueryRow(sqlStatement, username)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) SetUserSessionKey(username, key string) error {
	sqlStatement := "UPDATE users SET session_key=$1 WHERE username=$2;"
	_, err := d.connection.Exec(sqlStatement, key, username)
	return err
}

func (d *DataProvider) SetUserDescription(username, description string) error {
	sqlStatement := "UPDATE users SET description=$1 WHERE username=$2;"
	_, err := d.connection.Exec(sqlStatement, description, username)
	return err
}

func (d *DataProvider) GetUserDescription(username string) (string, error) {
	var result string
	sqlStatement := "SELECT description from users WHERE username=$1;"
	row := d.connection.QueryRow(sqlStatement, username)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) GetImageSecretHash(imageID int) (string, error) {
	var result string
	sqlStatement := "SELECT secret FROM images WHERE id=$1;"
	row := d.connection.QueryRow(sqlStatement, imageID)
	err := row.Scan(&result)
	if err == sql.ErrNoRows {
		return "", nil
	}
	return result, err
}

func (d *DataProvider) SetImageOwner(ctx context.Context, tx *sql.Tx, imageID int, owner string) error {
	var ownerID int
	row := tx.QueryRow("SELECT id FROM users WHERE username=$1;", owner)
	err := row.Scan(&ownerID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	_, err = tx.ExecContext(ctx, "UPDATE images SET owner_id=$1 WHERE id=$2;", ownerID, imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	_, err = tx.ExecContext(ctx, "INSERT INTO ownership_history (owner_id, image_id, time) VALUES ($1, $2, $3)", ownerID, imageID, time.Now())
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	_, err = tx.ExecContext(ctx, "DELETE FROM avatars WHERE image_id=$1;", imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return nil
}

func (d *DataProvider) ChangeImagePrivacy(user dto.User, imageID int) (bool, error) {
	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return false, err
	}

	var ownerName string
	row := tx.QueryRow("SELECT users.username FROM users JOIN images ON users.id=images.owner_id WHERE images.id=$1;", imageID)
	err = row.Scan(&ownerName)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	if ownerName != user.Username {
		return false, tx.Rollback()
	}

	var blocked bool
	row = tx.QueryRow("SELECT blocked FROM images WHERE id=$1", imageID)
	err = row.Scan(&blocked)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	if blocked {
		err = fmt.Errorf("image is blocked")
		return false, errors.Collapse(err, tx.Rollback())
	}

	_, err = tx.ExecContext(ctx, "UPDATE images SET is_private=(SELECT NOT is_private FROM images WHERE id=$1) WHERE id=$2;", imageID, imageID)
	if err != nil {
		return false, errors.Collapse(tx.Rollback())
	}

	_, err = tx.ExecContext(ctx, "DELETE FROM avatars WHERE image_id=$1;", imageID)
	if err != nil {
		return false, errors.Collapse(tx.Rollback())
	}

	return true, tx.Commit()
}

func (d *DataProvider) IsImagePrivate(imageID int) (bool, error) {
	var result bool
	sqlStatement := "SELECT is_private FROM images WHERE id=$1;"
	row := d.connection.QueryRow(sqlStatement, imageID)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) isUserOwnerOfImage(tx *sql.Tx, username string, imageID int) (bool, error) {
	var result bool
	sqlStatement := "SELECT EXISTS (SELECT * FROM users JOIN images ON users.id=images.owner_id WHERE images.id=$1 AND users.username=$2);"
	row := tx.QueryRow(sqlStatement, imageID, username)
	err := row.Scan(&result)
	return result, err
}

func (d *DataProvider) IsUserOwnerOfImage(username string, imageID int) (bool, error) {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	ok, err := d.isUserOwnerOfImage(tx, username, imageID)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	return ok, tx.Commit()
}

func (d *DataProvider) SaveImageComplaint(username string, imageID int) error {
	userID, err := d.GetUserID(username)
	if err != nil {
		return err
	}

	sqlStatement := "INSERT INTO complaints_image (user_id, image_id) VALUES ($1, $2)"
	_, err = d.connection.Exec(sqlStatement, userID, imageID)
	return err
}

func (d *DataProvider) DeleteImageComplaint(complaintID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return err
	}
	err = d.deleteImageComplaint(tx, complaintID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return tx.Commit()
}

func (d *DataProvider) ApproveImageComplaint(complaintID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return err
	}

	var imageID int
	sqlStatement := "SELECT image_id FROM complaints_image WHERE id=$1"

	row := tx.QueryRow(sqlStatement, complaintID)

	err = row.Scan(&imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	_, err = tx.Exec("UPDATE images SET (is_private, blocked) = (true, true) WHERE id=$1", imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	err = d.deleteImageComplaint(tx, complaintID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return tx.Commit()
}

func (d *DataProvider) deleteImageComplaint(tx *sql.Tx, complaintID int) error {
	sqlStatement := "DELETE FROM complaints_image WHERE id=$1"
	_, err := tx.Exec(sqlStatement, complaintID)
	return err
}

func (d *DataProvider) GetImageComplaints() ([]dto.ImageComplaint, error) {
	sqlStatement := "SELECT id, image_id FROM complaints_image"

	var complaints []dto.ImageComplaint
	rows, err := d.connection.Query(sqlStatement)
	if err != nil {
		return nil, err
	}
	var complaint dto.ImageComplaint
	for rows.Next() {
		err = rows.Scan(&complaint.ComplaintID, &complaint.ImageID)
		if err != nil {
			return nil, err
		}

		complaints = append(complaints, complaint)
	}
	err = rows.Close()

	return complaints, err
}

func (d *DataProvider) GetUsersImages(user dto.User) (dto.ImageFeed, error) {
	rows, err := d.connection.Query("SELECT id, image FROM images WHERE owner_id=(SELECT id FROM users WHERE username=$1);", user.Username)
	if err != nil {
		return nil, err
	}

	var id int
	var image []byte
	var posts []dto.ImagePost

	for rows.Next() {
		err = rows.Scan(&id, &image)
		if err != nil {
			return nil, err
		}

		posts = append(posts, dto.ImagePost{Image: image, ImageID: id})
	}
	err = rows.Close()

	return posts, err
}

func (d *DataProvider) SetUserAvatar(user dto.User, imageID int) error {
	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	var isOwner bool
	sqlStatement := "SELECT EXISTS (SELECT * FROM users JOIN images ON users.id=images.owner_id WHERE images.id=$1 AND users.username=$2);"
	row := tx.QueryRow(sqlStatement, imageID, user.Username)
	err = row.Scan(&isOwner)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	var isPrivate bool
	sqlStatement = "SELECT is_private FROM images WHERE id=$1;"
	row = tx.QueryRow(sqlStatement, imageID)
	err = row.Scan(&isPrivate)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	if !isOwner || isPrivate {
		error := tx.Rollback()
		if error != nil {
			return error
		}
		return errors.Error("User can not set this avatar")
	}

	sqlStatement = "INSERT INTO avatars (user_id, image_id) VALUES ((SELECT id FROM users WHERE username=$1), $2) ON CONFLICT (user_id) DO UPDATE SET image_id = $3;"
	_, err = tx.ExecContext(ctx, sqlStatement, user.Username, imageID, imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	err = tx.Commit()
	return err
}

func (d *DataProvider) GetUserAvatar(user dto.User) (dto.Image, bool, error) {
	var isAvatarExist bool
	sqlStatement := "SELECT EXISTS (SELECT image_id FROM avatars WHERE user_id=(SELECT id FROM users WHERE username=$1));"
	row := d.connection.QueryRow(sqlStatement, user.Username)
	err := row.Scan(&isAvatarExist)
	if err != nil {
		return dto.Image{}, isAvatarExist, err
	}

	if !isAvatarExist {
		return dto.Image{}, isAvatarExist, nil
	}

	var image dto.Image
	sqlStatement = "SELECT id, image FROM images WHERE id=(SELECT image_id FROM avatars WHERE user_id=(SELECT id FROM users WHERE username=$1));"
	row = d.connection.QueryRow(sqlStatement, user.Username)
	err = row.Scan(&image.ID, &image.Body)
	return image, isAvatarExist, err
}

func (d *DataProvider) RequestRecoverImageOwnership(imageID int, username, secretHash string) error {
	sqlStatement := "INSERT INTO image_recovery_requests (username, image_id, image_secret) VALUES ($1, $2, $3) ON CONFLICT (username, image_id) DO UPDATE SET image_secret=$3;"
	_, err := d.connection.Exec(sqlStatement, username, imageID, secretHash)
	return err
}

func (d *DataProvider) GetImageRecoveryRequests() ([]dto.ImageRecoveryRequest, error) {
	ret := make([]dto.ImageRecoveryRequest, 0)

	sqlSatement := "SELECT username, image_id FROM image_recovery_requests;"
	rows, err := d.connection.Query(sqlSatement)
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		buf := dto.ImageRecoveryRequest{}
		err = rows.Scan(&buf.Username, &buf.ImageID)
		if err != nil {
			return nil, err
		}
		ret = append(ret, buf)
	}

	return ret, rows.Close()
}

func (d *DataProvider) ApproveImageRecoveryRequest(imageID int, username string) (bool, error) {
	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	sqlStatement := "SELECT image_secret FROM image_recovery_requests WHERE image_id=$1 AND username=$2;"
	var imageSecret string
	row := tx.QueryRow(sqlStatement, imageID, username)
	err = row.Scan(&imageSecret)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	sqlStatement = "SELECT FROM images WHERE id=$1 AND secret=$2 LIMIT 1;"
	rows, err := tx.Query(sqlStatement, imageID, imageSecret)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	if !rows.Next() {
		return false, errors.Collapse(errors.Error("secret doesn't match"), tx.Rollback())
	}
	err = rows.Close()
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	err = d.SetImageOwner(ctx, tx, imageID, username)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	sqlStatement = "DELETE FROM image_recovery_requests WHERE image_id=$1 AND username=$2"
	_, err = tx.Exec(sqlStatement, imageID, username)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	return true, tx.Commit()
}

func (d *DataProvider) DenyImageRecoveryRequest(imageID int, username string) error {
	sqlStatement := "DELETE FROM image_recovery_requests WHERE image_id=$1 AND username=$2"
	_, err := d.connection.Exec(sqlStatement, imageID, username)
	return err
}

func (d *DataProvider) IsUserModerator(username string) (bool, error) {
	sqlStatement := "SELECT is_admin FROM users WHERE username=$1"
	row := d.connection.QueryRow(sqlStatement, username)
	isModerator := false
	err := row.Scan(&isModerator)
	return isModerator, err
}

func (d *DataProvider) RequestImageTransfer(senderUsername, receiverUsername string, imageID int) (bool, error) {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	ok, err := d.isUserOwnerOfImage(tx, senderUsername, imageID)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	if !ok {
		return false, tx.Commit()
	}
	sqlStatement := "INSERT INTO image_transfer_requests (sender_username, receiver_username, image_id) VALUES ($1, $2, $3) ON CONFLICT (image_id) DO UPDATE SET sender_username = $1, receiver_username = $2;"
	_, err = tx.Exec(sqlStatement, senderUsername, receiverUsername, imageID)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}
	return true, tx.Commit()
}

func (d *DataProvider) GetImageTransferRequests(receiverUsername string) ([]dto.ImageTransferRequest, error) {
	sqlStatement := "SELECT sender_username, receiver_username, image_id FROM image_transfer_requests WHERE receiver_username = $1;"
	ret := make([]dto.ImageTransferRequest, 0)
	rows, err := d.connection.Query(sqlStatement, receiverUsername)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		request := dto.ImageTransferRequest{}
		err = rows.Scan(&request.SenderUsername, &request.ReceiverUsername, &request.ImageID)
		if err != nil {
			return nil, err
		}
		ret = append(ret, request)
	}
	return ret, rows.Close()

}

func (d *DataProvider) ApproveImageTransferRequest(senderUsername, receiverUsername string, imageID int) (bool, string, error) {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}
	ok, err := d.isUserOwnerOfImage(tx, senderUsername, imageID)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}
	if !ok {
		return false, "", tx.Commit()
	}

	sqlStatement := "SELECT FROM image_transfer_requests where sender_username = $1 AND receiver_username = $2 and image_id = $3 LIMIT 1;"
	rows, err := tx.Query(sqlStatement, senderUsername, receiverUsername, imageID)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}
	if !rows.Next() {
		return false, "", errors.Collapse(
			errors.Error("request not found"),
			d.deleteImageTransferRequest(tx, senderUsername, receiverUsername, imageID),
			tx.Rollback(),
		)
	}
	err = rows.Close()
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	sqlStatement = "DELETE FROM image_transfer_requests where sender_username = $1 AND receiver_username = $2 and image_id = $3;"
	_, err = tx.Exec(sqlStatement, senderUsername, receiverUsername, imageID)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	err = d.SetImageOwner(context.Background(), tx, imageID, receiverUsername)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	key, err := d.keyGenerator.MakeKey()
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	keyHash, err := d.hasher.Hash(key)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	sqlStatement = "UPDATE images SET secret = $2 WHERE id = $1;"
	_, err = tx.Exec(sqlStatement, imageID, keyHash)
	if err != nil {
		return false, "", errors.Collapse(err, tx.Rollback())
	}

	return true, key, tx.Commit()
}

func (d *DataProvider) deleteImageTransferRequest(tx *sql.Tx, senderUsername, receiverUsername string, imageID int) error {
	sqlStatement := "DELETE FROM image_transfer_requests WHERE sender_username = $1 AND receiver_username = $2 AND image_id = $3;"
	_, err := tx.Exec(sqlStatement, senderUsername, receiverUsername, imageID)
	return err
}

func (d *DataProvider) DenyImageTransferRequest(senderUsername, receiverUsername string, imageID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}
	err = d.deleteImageTransferRequest(tx, senderUsername, receiverUsername, imageID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}
	return tx.Commit()
}

func (d *DataProvider) ChangePublicationRights(initiatorUsername string, targetUsername string) (bool, error) {
	ctx := context.Background()
	tx, err := d.connection.BeginTx(ctx, nil)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	var initiatorIsAdmin bool
	sqlStatement := "SELECT is_admin FROM users WHERE username = $1;"
	row := tx.QueryRow(sqlStatement, initiatorUsername)
	err = row.Scan(&initiatorIsAdmin)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	var targetIsAdmin bool
	row = tx.QueryRow(sqlStatement, targetUsername)
	err = row.Scan(&targetIsAdmin)
	if err != nil {
		return false, errors.Collapse(err, tx.Rollback())
	}

	if initiatorIsAdmin && !targetIsAdmin {
		sqlStatement = "UPDATE users SET publication_allowed=(SELECT NOT publication_allowed FROM users WHERE username=$1) WHERE username=$2;"
		_, err := tx.Exec(sqlStatement, targetUsername, targetUsername)
		if err != nil {
			return false, errors.Collapse(err, tx.Rollback())
		}

		return true, tx.Commit()
	}

	return false, tx.Rollback()
}

func (d *DataProvider) RequestCommentComplaint(user dto.User, commentID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	sqlSatement := "SELECT id FROM users WHERE username = $1;"
	row := tx.QueryRow(sqlSatement, user.Username)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	var userID int
	err = row.Scan(&userID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	sqlStatement := "INSERT INTO complaints (user_id, comment_id) VALUES ($1, $2);"
	_, err = tx.Exec(sqlStatement, userID, commentID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}
	return tx.Commit()
}

func (d *DataProvider) GetCommentsComplaints() ([]dto.CommentComplaint, error) {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return nil, errors.Collapse(err, tx.Rollback())
	}

	complaints := make([]dto.CommentComplaint, 0)

	sqlSatement := "SELECT user_id, comment_id FROM complaints;"
	rows, err := tx.Query(sqlSatement)
	if err != nil {
		return nil, errors.Collapse(err, tx.Rollback())
	}

	for rows.Next() {
		complain := dto.CommentComplaint{}

		err = rows.Scan(&complain.UserID, &complain.CommentID)
		if err != nil {
			return nil, err
		}

		complaints = append(complaints, complain)
	}

	err = rows.Close()
	if err != nil {
		return nil, errors.Collapse(err, tx.Rollback())
	}

	for i := range complaints {
		sqlSatement = "SELECT username FROM users WHERE id = $1;"
		row := tx.QueryRow(sqlSatement, &complaints[i].UserID)
		err = row.Scan(&complaints[i].RequesterUsername)
		if err != nil {
			return nil, errors.Collapse(err, tx.Rollback())
		}

		sqlSatement = "SELECT image_id, comment_text FROM comments WHERE id = $1;"
		row = tx.QueryRow(sqlSatement, complaints[i].CommentID)
		err = row.Scan(&complaints[i].ImageID, &complaints[i].Text)
		if err != nil {
			return nil, errors.Collapse(err, tx.Rollback())
		}
	}

	return complaints, tx.Commit()
}

func (d *DataProvider) DeleteCommentAndComplaints(commentID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	err = d.deleteCommentComplaints(tx, commentID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	sqlStatement := "DELETE FROM comments WHERE id = $1;"
	_, err = tx.Exec(sqlStatement, commentID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return tx.Commit()
}

func (d *DataProvider) DenyCommentComplaints(commentID int) error {
	tx, err := d.connection.BeginTx(context.Background(), nil)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	err = d.deleteCommentComplaints(tx, commentID)
	if err != nil {
		return errors.Collapse(err, tx.Rollback())
	}

	return tx.Commit()
}

func (d *DataProvider) deleteCommentComplaints(tx *sql.Tx, commentID int) error {
	sqlStatement := "DELETE FROM complaints WHERE comment_id = $1;"
	_, err := tx.Exec(sqlStatement, commentID)
	if err != nil {
		return err
	}

	return nil
}
