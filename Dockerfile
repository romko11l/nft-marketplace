FROM golang:1.18.0 as builder
WORKDIR /src/

COPY ./src/go.mod ./src/go.sum ./
RUN go mod download

COPY ./src/ /src/
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./bin/marketplace ./cmd/marketplace


FROM alpine:3.13.6

RUN apk --no-cache add ca-certificates
WORKDIR /marketplace/
COPY --from=builder /src/bin/marketplace /marketplace/
COPY ./static/ ./static/

RUN addgroup -g 12345 marketplace && \
    adduser --disabled-password -G marketplace -u 12345 marketplace
USER marketplace

HEALTHCHECK NONE

ENTRYPOINT ["./marketplace"]
