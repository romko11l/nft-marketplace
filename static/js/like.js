async function likeImage(image) {
    imgId = $(image).parent().attr("id");
    id = imgId.split('_')[1];

    await fetch("/like_image", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({"imageId": id}),
    }).then((response) => {
        if (response.ok) {
            $(image).parent().children("i").html("<p>You like this image!</p>");
        } else {
            $(image).parent().children("i").html("<p>You can not like this image!</p>");
        }
    });

    await fetch("/likes_amount?image_id=" + id).then((response) => {
        if (response.ok) {
            response.text().then((amount) => {
                $(image).parent().children("p").html(`<p>Likes: ${amount}</p>`)
            });
        }
    });
}
