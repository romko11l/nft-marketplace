\c marketplace

CREATE SEQUENCE user_ids;
CREATE TABLE users (
    id 		 	        INTEGER PRIMARY KEY DEFAULT nextval('user_ids'),
    username 	        TEXT UNIQUE NOT NULL,
    password_hash       TEXT NOT NULL,
    description         TEXT NOT NULL DEFAULT '',
    session_key         TEXT NOT NULL DEFAULT '',
    is_admin            BOOL NOT NULL DEFAULT FALSE,
    publication_allowed BOOL NOT NULL DEFAULT TRUE
);
ALTER SEQUENCE user_ids OWNED BY users.id;


CREATE SEQUENCE image_ids;
CREATE TABLE images (
    id 		   INTEGER PRIMARY KEY DEFAULT nextval('image_ids'),
    image_hash TEXT UNIQUE NOT NULL,
    owner_id   INTEGER NOT NULL,
    image      BYTEA NOT NULL,
    secret     TEXT NOT NULL,
    is_private BOOL NOT NULL DEFAULT TRUE,
    blocked    BOOL NOT NULL DEFAULT FALSE,

    FOREIGN KEY (owner_id) REFERENCES users(id)
);
ALTER SEQUENCE image_ids OWNED BY images.id;


CREATE SEQUENCE comment_ids;
CREATE TABLE comments (
    id 			  INTEGER PRIMARY KEY DEFAULT nextval('comment_ids'),
    author_id 	  INTEGER NOT NULL,
    image_id      INTEGER NOT NULL,
    comment_text  TEXT,
    creation_time TIMESTAMP NOT NULL,

    FOREIGN KEY (author_id) REFERENCES users(id),
    FOREIGN KEY (image_id)  REFERENCES images(id)
);
ALTER SEQUENCE comment_ids OWNED BY comments.id;


CREATE TABLE likes (
    user_id  INTEGER NOT NULL,
    image_id INTEGER NOT NULL,

    FOREIGN KEY (user_id)  REFERENCES users(id),
    FOREIGN KEY (image_id) REFERENCES images(id),

    CONSTRAINT one_user_one_like UNIQUE (image_id, user_id)
);


CREATE SEQUENCE ownership_history_ids;
CREATE TABLE ownership_history (
    id       INTEGER PRIMARY KEY DEFAULT nextval('ownership_history_ids'),
    owner_id INTEGER NOT NULL,
    image_id INTEGER NOT NULL,
    time     TIMESTAMP NOT NULL,

    FOREIGN KEY (owner_id) REFERENCES users(id),
    FOREIGN KEY (image_id) REFERENCES images(id)
);
ALTER SEQUENCE ownership_history_ids OWNED BY ownership_history.id;


CREATE SEQUENCE complaint_ids;
CREATE TABLE complaints (
    id          INTEGER PRIMARY KEY DEFAULT nextval('complaint_ids'),
    user_id     INTEGER NOT NULL,
    comment_id  INTEGER NOT NULL,

    FOREIGN KEY (user_id)    REFERENCES users(id),
    FOREIGN KEY (comment_id) REFERENCES comments(id),
    CONSTRAINT unique_complaint UNIQUE (user_id, comment_id)
);
ALTER SEQUENCE complaint_ids OWNED BY complaints.id;

CREATE SEQUENCE complaint_image_ids;
CREATE TABLE complaints_image (
    id          INTEGER PRIMARY KEY DEFAULT nextval('complaint_image_ids'),
    user_id     INTEGER NOT NULL,
    image_id    INTEGER NOT NULL,

    UNIQUE (user_id, image_id),
    FOREIGN KEY (user_id)  REFERENCES users(id),
    FOREIGN KEY (image_id) REFERENCES images(id)
);
ALTER SEQUENCE complaint_image_ids OWNED BY complaints_image.id;

CREATE TABLE friends (
    user1_id INTEGER NOT NULL,
    user2_id INTEGER NOT NULL,

    FOREIGN KEY (user1_id) REFERENCES users(id),
    FOREIGN KEY (user2_id) REFERENCES users(id),

    CONSTRAINT unique_friends UNIQUE (user1_id, user2_id),
    CONSTRAINT not_friend_for_himself CHECK (user1_id != user2_id)
);


CREATE TABLE friends_request (
    requesting_id INTEGER NOT NULL,
    purpose_id    INTEGER NOT NULL,

    FOREIGN KEY (requesting_id) REFERENCES users(id),
    FOREIGN KEY (purpose_id)    REFERENCES users(id),

    CONSTRAINT unique_request UNIQUE (requesting_id, purpose_id),
    CONSTRAINT not_friend_for_himself CHECK (requesting_id != purpose_id)
);


CREATE TABLE image_transfer_requests (
    sender_username   TEXT NOT NULL,
    receiver_username TEXT NOT NULL,
    image_id          INTEGER NOT NULL,

    FOREIGN KEY (sender_username)   REFERENCES users(username),
    FOREIGN KEY (receiver_username) REFERENCES users(username),
    FOREIGN KEY (image_id)          REFERENCES images(id),

    CONSTRAINT unique_transfer UNIQUE (image_id)
);

CREATE TABLE image_recovery_requests (
    username     TEXT NOT NULL,
    image_id     INTEGER NOT NULL,
    image_secret TEXT NOT NULL,

    FOREIGN KEY (username) REFERENCES users(username),
    FOREIGN KEY (image_id) REFERENCES images(id),

    CONSTRAINT unique_recovery UNIQUE (username, image_id)
);


CREATE TABLE avatars (
    user_id  INTEGER NOT NULL,
    image_id INTEGER NOT NULL,

    FOREIGN KEY (user_id)  REFERENCES users(id),
    FOREIGN KEY (image_id) REFERENCES images(id),

    CONSTRAINT one_user_one_avatar UNIQUE (user_id),
    CONSTRAINT one_avatar_one_user UNIQUE (image_id)
);
